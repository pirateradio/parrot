import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export interface LayoutProps {
  window: WindowProps;
  isSmallDevice: boolean;
}

export interface WindowProps {
  width: number;
  height: number;
}

const Layout: LayoutProps = {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
};

export default Layout;
