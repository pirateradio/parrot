const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';

export type ColorKeys = 'light' | 'dark';

export interface ColorProps {
  text: string;
  base0: string;
  base1: string;
  base2: string;
  base3: string;
  border: string;
  primary: string;
  secondary: string;
  inactive: string;
  inverted: string;
  light: string;
  dark: string;
  tint: string;
  red: string;
  green: string;
  lightGreen: string;
  tabIconDefault: string;
  tabIconSelected: string;
  transparent: 'transparent';
  white: string;
}

export type Colors = Record<ColorKeys, ColorProps>;

const colors: Colors = {
  light: {
    text: '#000',
    base0: '#ffffff', // background
    base1: '#eeeeee', // headers/footers and shapes with background tint
    base2: '#dddddd', //
    base3: '#cccccc', //
    border: '#eeeeee',
    primary: '#000000cc', // primary text and icons
    secondary: '#00000066', // secondary text and icons
    inactive: '#00000033', // disabled text and icons
    inverted: '#ffffffee', // inverted text on non-base background
    light: '#ffffffee', // light color regardless of theme
    dark: '#000000cc', // dark color regardless of theme
    tint: tintColorLight,
    red: '#A53741',
    green: '#6e9c32', // for confirmations or positive actions (save)
    lightGreen: '#9ecd63', // light green
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    transparent: 'transparent',
    white: '#fff',
  },
  dark: {
    text: '#fff',
    base0: '#101019',
    base1: '#1f1f28',
    base2: '#2e2e37',
    base3: '#3d3d46',
    border: '#252525',
    primary: '#ffffffee',
    secondary: '#ffffff99',
    inactive: '#ffffff44',
    inverted: '#00000033',
    light: '#ffffffee',
    dark: '#000000cc',
    tint: tintColorDark,
    red: '#db676c',
    green: '#9ecd63',
    lightGreen: '#d1ff93',
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    transparent: 'transparent',
    white: '#fff',
  },
};

export default colors;

/**
 * Created a few types to help define colors so that they could be shared with functions in the
 * Themed file. It also helps to keep everything that is referencing the colors object aligned
 * to it and makes expanding the colors object with additional definitions in the future an
 * easier task.
 */
