import { createContext } from 'react';
import { action, computed, makeObservable, observable, runInAction } from 'mobx';
import * as SecureStore from 'expo-secure-store';
import * as AuthSession from 'expo-auth-session';
import * as Sentry from 'sentry-expo';

import callApi from '../utils/api';
import { UserAccount } from '../types';

class Auth {
  baseUrl = '';
  clientId = '';
  clientSecret = '';
  accessCode = '';
  accessToken = '';
  refreshToken = '';
  isInit = false;

  constructor() {
    makeObservable(this, {
      accessToken: observable,
      isInit: observable,
      init: action,
      refreshAccessToken: action,
      loggedIn: computed,
    });
  }

  async init() {
    try {
      const [baseUrl, clientId, clientSecret, accessCode, accessToken, refreshToken] = await Promise.all([
        SecureStore.getItemAsync('baseUrl'),
        SecureStore.getItemAsync('clientId'),
        SecureStore.getItemAsync('clientSecret'),
        SecureStore.getItemAsync('accessCode'),
        SecureStore.getItemAsync('accessToken'),
        SecureStore.getItemAsync('refreshToken'),
      ]);
      runInAction(() => {
        this.accessToken = accessToken || '';
      });
      this.baseUrl = baseUrl || '';
      this.clientId = clientId || '';
      this.clientSecret = clientSecret || '';
      this.accessCode = accessCode || '';
      this.refreshToken = refreshToken || '';
      if (this.accessToken) {
        const codeWorks = await this.verifyAccessToken();
        if (!codeWorks) {
          runInAction(() => {
            this.accessToken = '';
          });
        }
      }
      runInAction(() => {
        this.isInit = true;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  get loggedIn() {
    return !!(this.isInit && this.accessToken);
  }

  async verifyAccessToken() {
    try {
      const accountDetails = await callApi<UserAccount>(this.baseUrl, 'accounts/verify_credentials', this.accessToken);
      return !!accountDetails.acct;
    } catch (err) {
      Sentry.Native.captureException(err);
      return false;
    }
  }

  async registerClient(baseUrl: string) {
    try {
      this.baseUrl = baseUrl;
      const redirect_uris = [AuthSession.makeRedirectUri()];
      const scopes = ['read', 'write', 'follow'];
      const res = await fetch(`${baseUrl}/api/v1/apps`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          client_name: 'Parrot',
          redirect_uris: redirect_uris.join('\n'),
          scopes: scopes.join(' '),
        }),
      });
      const { client_id, client_secret } = await res.json();
      this.clientId = client_id;
      this.clientSecret = client_secret;
      await Promise.all([
        SecureStore.setItemAsync('clientId', client_id),
        SecureStore.setItemAsync('clientSecure', client_secret),
        SecureStore.setItemAsync('baseUrl', baseUrl),
      ]);
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async promptUserAndLogin() {
    try {
      const request = new AuthSession.AuthRequest(genAuthConfig(this.clientId));
      const response = await request.promptAsync(genAuthDiscovery(this.baseUrl));
      if (response.type === 'success') {
        const { code } = response.params;
        SecureStore.setItemAsync('accessCode', code);
        return this.fetchAccessToken(code);
      }
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async fetchAccessToken(accessCode: string) {
    try {
      const { accessToken, refreshToken } = await AuthSession.exchangeCodeAsync(
        {
          ...genAuthConfig(this.clientId),
          code: accessCode,
          extraParams: {
            client_secret: this.clientSecret,
          },
        },
        genAuthDiscovery(this.baseUrl)
      );
      runInAction(() => {
        this.accessCode = accessCode;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken || '';
      });
      await Promise.all([
        SecureStore.setItemAsync('accessToken', this.accessToken),
        SecureStore.setItemAsync('refreshToken', this.refreshToken),
      ]);
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  // not working. unsure why
  async refreshAccessToken() {
    if (!this.refreshToken) return; // not supported
    try {
      const { accessToken, refreshToken } = await AuthSession.refreshAsync(
        {
          ...genAuthConfig(this.clientId),
          clientSecret: this.clientSecret,
          refreshToken: this.refreshToken,
        },
        genAuthDiscovery(this.baseUrl)
      );
      runInAction(() => {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken || '';
      });
      await Promise.all([
        SecureStore.setItemAsync('accessToken', this.accessToken),
        SecureStore.setItemAsync('refreshToken', this.refreshToken),
      ]);
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }
}

export const genAuthDiscovery = (baseUrl: string): AuthSession.DiscoveryDocument => ({
  authorizationEndpoint: `${baseUrl}/oauth/authorize`,
  tokenEndpoint: `${baseUrl}/oauth/token`,
  revocationEndpoint: `${baseUrl}/oauth/revoke`,
});

export const genAuthConfig = (clientId: string): AuthSession.AuthRequestConfig => ({
  clientId,
  redirectUri: AuthSession.makeRedirectUri(),
  scopes: ['read', 'write', 'follow'],
  responseType: 'code',
});

export const auth = new Auth();

export const AuthContext = createContext<Auth>(auth);
