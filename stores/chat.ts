import { action, makeObservable, observable, runInAction } from 'mobx';
import * as Sentry from 'sentry-expo';
import { Chat as ChatType, ChatID, ChatMessage, UserAccount } from '../types';
import { auth } from './auth';
import callApi from '../utils/api';

export default class Chat {
  otherUser?: UserAccount = undefined;
  chatId: ChatID = '';
  messages: ChatMessage[] = [];
  refreshing = false;
  isInit = false;

  constructor(chatId: ChatID) {
    this.chatId = chatId;
    makeObservable(this, {
      messages: observable,
      refreshing: observable,
      isInit: observable,
      otherUser: observable,
      init: action,
      addMessage: action,
    });
    this.init();
  }

  async init() {
    await this.loadChatMessages();
    runInAction(() => {
      this.isInit = true;
    });
  }

  async loadChatMessages() {
    try {
      this.refreshing = true;
      const [{ account }, messages] = await Promise.all([
        callApi<ChatType>(auth.baseUrl, `pleroma/chats/${this.chatId}`, auth.accessToken),
        callApi<ChatMessage[]>(auth.baseUrl, `pleroma/chats/${this.chatId}/messages`, auth.accessToken),
      ]);
      runInAction(() => {
        this.otherUser = account;
        this.messages = messages;
        this.refreshing = false;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async addMessage(message: ChatMessage) {
    this.messages = [message, ...this.messages];
  }
}
