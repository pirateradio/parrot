import { makeObservable, observable, action, runInAction, computed } from 'mobx';
import { createContext } from 'react';
import * as Sentry from 'sentry-expo';

import callApi from '../utils/api';
import { UserAccount } from '../types';
import { auth } from '../stores/auth';

export class User {
  account: UserAccount | undefined = undefined;
  isInit = false;

  constructor() {
    makeObservable(this, {
      account: observable,
      isInit: observable,
      init: action,
      refreshUserAccount: action,
      id: computed,
    });
  }

  async init() {
    await this.refreshUserAccount();
    runInAction(() => {
      this.isInit = true;
    });
  }

  get id() {
    return this.account?.id || '';
  }

  async refreshUserAccount() {
    try {
      const account = await callApi<UserAccount>(auth.baseUrl, 'accounts/verify_credentials', auth.accessToken);
      Sentry.Native.setUser({
        username: `${account.acct}@${auth.baseUrl.toLowerCase().replace('https://', '').replace('http://', '')}`,
      });
      runInAction(() => {
        this.account = account;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }
}

export const user = new User();

export const UserContext = createContext<User>(user);
