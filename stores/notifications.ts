import { createContext } from 'react';
import { observable, makeObservable, action, runInAction } from 'mobx';
import * as Sentry from 'sentry-expo';
import { Notification } from '../types';
import { auth } from './auth';
import callApi from '../utils/api';

export default class Notifications {
  list: Notification[] = [];
  refreshing = false;
  isInit = false;

  constructor() {
    makeObservable(this, {
      list: observable,
      refreshing: observable,
      isInit: observable,
      init: action,
      loadNotificationList: action,
    });
  }

  async init() {
    await this.loadNotificationList();
    runInAction(() => {
      this.isInit = true;
    });
  }

  async loadNotificationList() {
    try {
      this.refreshing = true;
      const notifications = await callApi<Notification[]>(auth.baseUrl, 'notifications', auth.accessToken);
      runInAction(() => {
        this.list = [...notifications];
        this.refreshing = false;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }
}

export const notifications = new Notifications();

export const NotificationsContext = createContext(notifications);
