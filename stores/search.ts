import * as React from 'react';
import { action, makeObservable, observable, runInAction } from 'mobx';
import * as Sentry from 'sentry-expo';
import { auth } from './auth';
import { UserAccount, Status } from '../types';
import callApi, { ApiCallOptions } from '../utils/api';

export default class Search {
  accounts = observable<UserAccount>([]);
  hashtags = observable<string>([]);
  statuses = observable<Status>([]);
  loading = false;
  isInit = false;

  constructor() {
    makeObservable(this, {
      loading: observable,
      isInit: observable,
      runQuery: action,
    });
    this.isInit = true; // this is here so that we can use a pattern similar to other stores if we need an async setup (search 'async init()')
  }

  async runQuery(query: string) {
    this.loading = true;
    try {
      const params: ApiCallOptions['params'] = { q: query, resolve: true };
      const { accounts, hashtags, statuses } = await callApi<{
        accounts: UserAccount[];
        hashtags: string[];
        statuses: Status[];
      }>(auth.baseUrl, `search`, auth.accessToken, { params });
      runInAction(() => {
        this.accounts.replace(accounts);
        this.hashtags.replace(hashtags);
        this.statuses.replace(statuses);
        this.loading = false;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }
}

export const search = new Search();

export const SearchContext = React.createContext<Search>(search);
