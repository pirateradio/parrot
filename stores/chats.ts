import { createContext } from 'react';
import { action, makeObservable, observable, runInAction } from 'mobx';
import * as Sentry from 'sentry-expo';
import { auth } from './auth';
import callApi from '../utils/api';
import { Chat } from '../types';

export default class Chats {
  list: Chat[] = [];
  refreshing = false;
  isInit = false;

  constructor() {
    makeObservable(this, {
      list: observable,
      isInit: observable,
      refreshing: observable,
      init: action,
      loadChatList: action,
      updateChat: action,
    });
  }

  async init() {
    try {
      await this.loadChatList();
      runInAction(() => {
        this.isInit = true;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async loadChatList() {
    try {
      this.refreshing = true;
      const chats = await callApi<Array<Chat>>(auth.baseUrl, `pleroma/chats`, auth.accessToken);
      runInAction(() => {
        this.list = [...chats];
        this.refreshing = false;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  updateChat(chat: Chat) {
    const index = this.list.findIndex(({ id }) => id === chat.id);
    if (index > -1) {
      this.list[index] = chat;
      this.list = [...this.list];
    } else {
      this.list = [chat, ...this.list];
    }
  }
}

export const chats = new Chats();

export const ChatsContext = createContext(chats);
