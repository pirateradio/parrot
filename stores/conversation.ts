import { action, makeObservable, observable, runInAction } from 'mobx';
import * as Sentry from 'sentry-expo';
import { auth } from './auth';
import callApi from '../utils/api';
import { ConversationID, StatusContext, Status } from '../types';
import { timeline } from './timeline';

class Conversation {
  conversationId: ConversationID = ''; // this is the status that was clicked on to get into the chat
  messages = observable.array<Status>([], { deep: false });
  isInit = false;
  refreshing = false;

  constructor(conversationId: ConversationID) {
    this.conversationId = conversationId;
    makeObservable(this, {
      conversationId: observable,
      isInit: observable,
      refreshing: observable,
      init: action,
      removeMessage: action,
      addMessage: action,
    });
    this.init();
  }

  async init() {
    try {
      await this.loadConversation();
      runInAction(() => {
        this.isInit = true;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async loadConversation() {
    try {
      this.refreshing = true;
      let status = timeline.conversations.find(({ id }) => id === this.conversationId); // we already have the status loaded from timeline
      if (!status) status = await callApi<Status>(auth.baseUrl, `statuses/${this.conversationId}`, auth.accessToken);
      const { ancestors, descendants } = await callApi<StatusContext>(
        auth.baseUrl,
        `statuses/${this.conversationId}/context`,
        auth.accessToken
      );
      const messages = [...ancestors, { ...status }, ...descendants].filter(({ reblog }) => !reblog);
      runInAction(() => {
        this.messages.replace(messages);
        this.refreshing = false;
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  removeMessage(messageId: Status['id']) {
    this.messages.replace(this.messages.filter(({ id }) => id !== messageId));
  }

  addMessage(message: Status) {
    this.messages.replace([...this.messages, message]);
  }
}

export default Conversation;
