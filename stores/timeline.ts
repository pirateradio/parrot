import { createContext } from 'react';
import { observable, makeObservable, action, runInAction, computed } from 'mobx';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Sentry from 'sentry-expo';
import uniqBy from 'lodash/uniqBy';
import { auth } from './auth';
import { Status } from '../types';
import callApi, { ApiCallOptions } from '../utils/api';
import { user } from './user';

export type TimelineOptionsList = {
  home: undefined;
  local: undefined;
  public: undefined;
  favorites: undefined;
  bookmarks: undefined;
  showBoosts: undefined;
  showMentions: undefined;
  showFriendReplies: undefined;
  showMyReplies: undefined;
  showDMs: undefined;
  mediaOnly: undefined;
};

type OptionsFlags<Type> = {
  [Property in keyof Type]?: boolean;
};
type FilterSettings = OptionsFlags<TimelineOptionsList>;

export default class Timeline {
  conversations = observable.array<Status>([]);
  isInit = false;
  refreshing = false;
  timelineOption: keyof TimelineOptionsList = 'home';
  filters: FilterSettings = {
    showBoosts: true,
    showMentions: true,
    showFriendReplies: true,
    showMyReplies: false,
    showDMs: true,
    mediaOnly: false,
  };

  constructor() {
    makeObservable(this, {
      isInit: observable,
      refreshing: observable,
      filters: observable,
      filteredConversations: computed,
      init: action,
      loadConversations: action,
      addConversation: action,
      setTimelineOption: action,
      toggleFilter: action,
      loadMoreConversations: action,
    });
  }

  async init() {
    const [filters, timelineOption] = await Promise.all([
      AsyncStorage.getItem('filters'),
      AsyncStorage.getItem('timelineOption'),
    ]);
    runInAction(() => {
      if (timelineOption) this.timelineOption = timelineOption as keyof TimelineOptionsList;
      if (filters) this.filters = { ...this.filters, ...JSON.parse(filters) };
    });
    await this.loadConversations();
    runInAction(() => {
      this.isInit = true;
    });
  }

  get filteredConversations() {
    if (['favorites', 'bookmarks'].includes(this.timelineOption)) return this.conversations;
    const { showBoosts, showDMs, showFriendReplies, showMentions, showMyReplies } = this.filters;
    return this.conversations.filter(({ visibility, reblog, account: { id: userId }, in_reply_to_id, mentions }) => {
      if (!showDMs && visibility === 'direct') return false;
      if (!showBoosts && reblog && userId !== user.id) return false;
      if (in_reply_to_id) {
        if (showMyReplies && userId === user.id) return true;
        if (!showFriendReplies && userId !== user.id) return false;
        if (!showMentions && !mentions.some(({ id }) => id === userId)) return false;
      }
      return true;
    });
  }

  toggleFilter(filter: keyof FilterSettings) {
    this.filters = {
      ...this.filters,
      [filter]: !this.filters[filter],
    };
    AsyncStorage.setItem('filters', JSON.stringify(this.filters));
    if (filter === 'mediaOnly') this.loadConversations();
  }

  setTimelineOption(option: keyof TimelineOptionsList) {
    this.timelineOption = option;
    AsyncStorage.setItem('timelineOption', option);
  }

  async loadConversations() {
    this.refreshing = true;
    try {
      const params: ApiCallOptions['params'] = { limit: 20 };
      let endpoint;
      if (this.timelineOption === 'favorites') {
        endpoint = 'favourites';
      } else if (this.timelineOption === 'bookmarks') {
        endpoint = 'bookmarks';
      } else {
        endpoint = 'timelines/';
        if (['local', 'public'].includes(this.timelineOption)) {
          endpoint += `public`;
          params.local = this.timelineOption === 'local';
        } else endpoint += 'home';
        if (this.filters.mediaOnly) params['only_media'] = true;
      }
      const statuses = await callApi<Array<Status>>(auth.baseUrl, endpoint, auth.accessToken, { params });
      runInAction(() => {
        this.conversations.replace(statuses);
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
    runInAction(() => {
      this.refreshing = false;
    });
  }

  async loadMoreConversations() {
    if (!this.conversations.length) return;
    const { id: maxId } = this.conversations[this.conversations.length - 1];
    try {
      const params: ApiCallOptions['params'] = { limit: 20, max_id: maxId };
      let endpoint;
      if (this.timelineOption === 'favorites') {
        endpoint = 'favourites';
      } else if (this.timelineOption === 'bookmarks') {
        endpoint = 'bookmarks';
      } else {
        endpoint = 'timelines/';
        if (['local', 'public'].includes(this.timelineOption)) {
          endpoint += `public`;
          params.local = this.timelineOption === 'local';
        } else endpoint += 'home';
      }
      const statuses = await callApi<Array<Status>>(auth.baseUrl, endpoint, auth.accessToken, { params });
      runInAction(() => {
        const uniqueConversations = uniqBy([...this.conversations, ...statuses], ({ id }) => id);
        this.conversations.replace(uniqueConversations);
      });
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  addConversation(status: Status) {
    this.conversations.replace([status, ...this.conversations]);
  }

  async setFavorite(statusId: Status['id'], favorited: boolean) {
    try {
      const updatedStatus = await callApi<Status>(
        auth.baseUrl,
        `statuses/${statusId}/${favorited ? 'favourite' : 'unfavourite'}`,
        auth.accessToken,
        { method: 'POST' }
      );
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async setBookmark(statusId: Status['id'], bookmarked: boolean) {
    try {
      const updatedStatus = await callApi<Status>(
        auth.baseUrl,
        `statuses/${statusId}/${bookmarked ? 'bookmark' : 'unbookmark'}`,
        auth.accessToken,
        { method: 'POST' }
      );
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async setBoost(statusId: Status['id'], boosted: boolean) {
    try {
      const updatedStatus = await callApi<Status>(
        auth.baseUrl,
        `statuses/${statusId}/${boosted ? 'reblog' : 'unreblog'}`,
        auth.accessToken,
        { method: 'POST' }
      );
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async setEmojiReaction(statusId: Status['id'], emoji: string) {
    try {
      const updatedStatus = await callApi<Status>(
        auth.baseUrl,
        `pleroma/statuses/${statusId}/reactions/${emoji}`,
        auth.accessToken,
        { method: 'PUT' }
      );
    } catch (err) {
      Sentry.Native.captureException(err);
      throw new Error('Unsupported Emoji');
    }
  }

  async removeEmojiReaction(statusId: Status['id'], emoji: string) {
    try {
      const updatedStatus = await callApi<Status>(
        auth.baseUrl,
        `pleroma/statuses/${statusId}/reactions/${emoji}`,
        auth.accessToken,
        { method: 'DELETE' }
      );
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async setPin(statusId: Status['id'], pin: boolean) {
    try {
      const updatedStatus = await callApi<Status>(
        auth.baseUrl,
        `statuses/${statusId}/${pin ? 'pin' : 'unpin'}`,
        auth.accessToken,
        { method: 'POST' }
      );
      const index = this.conversations.findIndex(({ id }) => id === statusId);
      if (index > -1) {
        runInAction(() => (this.conversations[index] = { ...updatedStatus }));
      }
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async setMute(statusId: Status['id'], mute: boolean) {
    try {
      const updatedStatus = await callApi<Status>(
        auth.baseUrl,
        `statuses/${statusId}/${mute ? 'mute' : 'unmute'}`,
        auth.accessToken,
        { method: 'POST' }
      );
      const index = this.conversations.findIndex(({ id }) => id === statusId);
      if (index > -1) {
        runInAction(() => (this.conversations[index] = { ...updatedStatus }));
      }
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }

  async deleteStatus(statusId: Status['id']) {
    try {
      await callApi<Status>(auth.baseUrl, `statuses/${statusId}`, auth.accessToken, { method: 'DELETE' });
      runInAction(() => this.conversations.replace(this.conversations.filter(({ id }) => id !== statusId)));
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }
}

export const timeline = new Timeline();

export const TimelineContext = createContext<Timeline>(timeline);
