import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { StyleSheet } from 'react-native';
import { observer } from 'mobx-react-lite';
import unescape from 'lodash/unescape';
import { GiftedChat } from '@pirateradio/react-native-gifted-chat';

import { ChatMessage, ChatsTabParamList, PMessage } from '../types';
import { View } from '../components/Themed';
import InputActions from '../components/InputToolbar';
import Bubble from '../components/GiftedChat/Bubble';
import useUser from '../hooks/useUser';
import useChat from '../hooks/useChat';

// will need to parse mentions and emojis and stuff
// either here or in render chat bubble

const ChatScreen: React.FC<StackScreenProps<ChatsTabParamList, 'Chat'>> = ({ route }) => {
  const { chatId } = route.params;
  const [gcMessages, setGcMessages] = React.useState<Array<PMessage>>([]);
  const {
    chat: { messages, otherUser },
    loading,
  } = useChat(chatId, {
    onUpdate: () => null,
  });
  const user = useUser();
  const statusToGcMessage: (message: ChatMessage) => PMessage = React.useMemo(
    () => (message) => {
      const { id, account_id, created_at, attachment, content, emojis, card } = message;
      return {
        _id: id,
        text: unescape(content || ''),
        emojis,
        createdAt: new Date(created_at),
        user: {
          _id: account_id,
          name: user.id === account_id ? user.account?.display_name : otherUser?.display_name,
          avatar: user.id === account_id ? user.account?.avatar_static : otherUser?.avatar_static,
          emojis: user.id === account_id ? user.account?.emojis || [] : otherUser?.emojis || [],
        },
        mentions: [],
        attachments: attachment ? [attachment] : [],
        card,
        sensitive: false,
      };
    },
    [otherUser]
  );

  React.useEffect(() => {
    if (loading) return;
    setGcMessages(GiftedChat.append([], messages.map(statusToGcMessage)));
  }, [loading]);

  return (
    <View style={styles.container}>
      <GiftedChat
        messages={gcMessages}
        user={{ _id: user.id }}
        renderActions={(props) => <InputActions {...props} />}
        renderBubble={(props) => <Bubble {...props} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default observer(ChatScreen);
