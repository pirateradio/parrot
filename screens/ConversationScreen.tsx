import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { StyleSheet } from 'react-native';
import { observer } from 'mobx-react-lite';
import { GiftedChat } from '@pirateradio/react-native-gifted-chat';

import { TimelineTabParamList, Status, PMessage } from '../types';
import { View } from '../components/Themed';
import InputActions from '../components/InputToolbar';
import Bubble from '../components/GiftedChat/Bubble';
import useUser from '../hooks/useUser';
import useConversation from '../hooks/useConversation';

// will need to parse mentions and emojis and stuff
// either here or in render chat bubble
const statusToGcMessage: (status: Status) => PMessage = ({
  id,
  content,
  account,
  created_at,
  media_attachments,
  mentions,
  emojis,
  card,
  sensitive,
}) => {
  return {
    _id: id,
    text: content || '',
    mentions,
    emojis,
    createdAt: new Date(created_at),
    user: {
      _id: account['id'],
      name: account['acct'],
      ...account,
    },
    attachments: media_attachments,
    card,
    sensitive,
  };
};

const ConversationScreen: React.FC<StackScreenProps<TimelineTabParamList, 'Conversation'>> = ({ route }) => {
  const { convoId } = route.params;
  const [gcMessages, setGcMessages] = React.useState<Array<PMessage>>([]);
  const user = useUser();
  const {
    conversation: { messages },
    loading,
  } = useConversation(convoId, {
    // not sure to do these here, or to have it take place in conversation store, and re-map the conversation messages here when they change
    onUpdate: (newMessage) => {
      setGcMessages((prevMessages) => GiftedChat.append([statusToGcMessage(newMessage)], prevMessages));
    },
    onDelete: (statusId) => {
      if (gcMessages.findIndex(({ _id }) => _id === statusId) > -1) {
        setGcMessages((prevMessages) => {
          const messageIndex = prevMessages.findIndex(({ _id }) => _id === statusId);
          if (messageIndex > -1) {
            const { _id, createdAt, user, mentions, attachments, emojis, card, sensitive } = prevMessages[messageIndex];
            prevMessages[messageIndex] = {
              _id: `deleted_${_id}`,
              createdAt,
              text: `${user.name}'s message was deleted`,
              system: true,
              user,
              mentions,
              attachments,
              emojis,
              card,
              sensitive,
            };
          }

          return [...prevMessages];
        });
      }
    },
  });

  React.useEffect(() => {
    if (loading) return;
    setGcMessages(GiftedChat.append([], messages.map(statusToGcMessage)));
  }, [loading]);

  const onSend = React.useCallback((messages: PMessage[] = []) => {
    console.log(messages);
  }, []);

  return (
    <View style={styles.container}>
      <GiftedChat
        messages={gcMessages}
        user={{ _id: user.id }}
        inverted={false}
        onSend={onSend}
        renderActions={(props) => <InputActions {...props} />}
        renderBubble={(props) => <Bubble {...props} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default observer(ConversationScreen);
