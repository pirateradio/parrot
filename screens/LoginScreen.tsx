import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { StyleSheet } from 'react-native';
import { observer } from 'mobx-react-lite';
import * as WebBrowser from 'expo-web-browser';
import * as SplashScreen from 'expo-splash-screen';
import * as Sentry from 'sentry-expo';

import { View, TextInput } from '../components/Themed';
import RoundButton from '../components/RoundButton';
import { RootStackParamList } from '../types';
import useAuth from '../hooks/useAuth';
import useUser from '../hooks/useUser';

const LoginScreen: React.FC<StackScreenProps<RootStackParamList, 'Login'>> = ({ navigation }) => {
  const auth = useAuth();
  const user = useUser();
  const [baseUrl, setBaseUrl] = React.useState('https://pirateradio.social');
  const [loading, setLoading] = React.useState(true);
  React.useEffect(() => {
    const initUserAndNavHome = async () => {
      try {
        setLoading(true);
        await user.init();
        navigation.replace('Root');
      } catch (err) {
        Sentry.Native.captureException(err);
        setLoading(false);
      }
    };
    if (auth.loggedIn) initUserAndNavHome();
    else {
      SplashScreen.hideAsync();
      setLoading(false);
    }
  }, [auth.loggedIn]);

  React.useEffect(() => {
    WebBrowser.warmUpAsync();

    return () => {
      WebBrowser.coolDownAsync();
    };
  }, []);

  const handleLogin = React.useCallback(async () => {
    try {
      await auth.registerClient(baseUrl);
      await auth.promptUserAndLogin();
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }, [baseUrl]);

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer} colorName="base1">
        <TextInput
          autoFocus
          editable={!loading}
          style={styles.input}
          value={baseUrl}
          onChangeText={(u) => setBaseUrl(u.trim().toLowerCase())}
          autoCorrect={false}
          placeholder="https://my-pleroma-server.tld"
          textContentType="URL"
          returnKeyType="go"
          onSubmitEditing={handleLogin}
        />
      </View>
      <RoundButton text="Log In" disabled={loading} onPress={handleLogin} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  inputContainer: {
    paddingVertical: 3,
    paddingHorizontal: 15,
    borderRadius: 5,
    marginBottom: 10,
    flexDirection: 'row',
  },
  input: {
    height: 34,
    fontSize: 14,
    lineHeight: 16,
    margin: 0,
    fontWeight: '500',
    flex: 1,
  },
});

export default observer(LoginScreen);
