import { StackScreenProps } from '@react-navigation/stack';
import { observer } from 'mobx-react-lite';
import * as React from 'react';
import { StyleSheet, SectionList, SectionListRenderItem } from 'react-native';
import Constants from 'expo-constants';

import { View, Ionicons, IoniconsProps, Switch, Text } from '../components/Themed';
import ListItem from '../components/ListItem';
import SectionHeader from '../components/SectionHeader';

import { TimelineTabParamList } from '../types';
import { TimelineOptionsList } from '../stores/timeline';
import useTimeline from '../hooks/useTimeline';

interface OptionItem {
  name: string;
  description: string;
  value: keyof TimelineOptionsList;
  icon: IoniconsProps['name'];
}
interface OptionSection {
  title: string;
  data: OptionItem[];
  renderItem: SectionListRenderItem<OptionItem, OptionSection>;
}

const TimelineOptionsScreen: React.FC<StackScreenProps<TimelineTabParamList, 'TimelineOptions'>> = ({ navigation }) => {
  const {
    timeline,
    timeline: { filters },
  } = useTimeline({});

  const handleTimelinePress = React.useCallback((value: OptionItem['value']) => {
    timeline.setTimelineOption(value);
    timeline.loadConversations();
    navigation.navigate('Timeline');
  }, []);

  const timelineSection = React.useMemo<OptionSection>(
    () => ({
      title: 'Timeline',
      data: [
        {
          name: 'Home',
          description: 'Posts from users you follow',
          value: 'home',
          icon: 'ios-home',
        },
        {
          name: 'Local',
          description: 'Public posts from local users',
          value: 'local',
          icon: 'ios-people',
        },
        {
          name: 'Public',
          description: 'Public posts from users followed by local users',
          value: 'public',
          icon: 'ios-planet',
        },
        {
          name: 'Favorites',
          description: 'Posts that you have favorited',
          value: 'favorites',
          icon: 'ios-heart',
        },
        {
          name: 'Bookmarks',
          description: 'Posts that you have bookmarked',
          value: 'bookmarks',
          icon: 'ios-bookmark',
        },
      ],
      renderItem: ({ item }) => (
        <ListItem
          onPress={() => handleTimelinePress(item.value)}
          selected={timeline.timelineOption === item.value}
          maintext={item.name}
          subtext={item.description}
          leftSide={<Ionicons name={item.icon} size={30} />}
        />
      ),
    }),
    []
  );

  const timelineOptionsSettings = React.useMemo<OptionSection>(
    () => ({
      title: 'Timeline Filters',
      data: [
        {
          name: 'Show Boosts',
          description: 'See posts boosted by users you follow',
          value: 'showBoosts',
          icon: 'ios-rocket',
        },
        {
          name: 'Only Show Media',
          description: 'Only see posts with attached media',
          value: 'mediaOnly',
          icon: 'ios-attach',
        },
        {
          name: 'Show Direct Messages',
          description: '',
          value: 'showDMs',
          icon: 'ios-mail',
        },
        {
          name: 'Show Replies That Mention Me',
          description: 'See replies that mention me',
          value: 'showMentions',
          icon: 'ios-at-circle',
        },
        {
          name: "Show Friend's Replies",
          description: 'See replies made by users you follow',
          value: 'showFriendReplies',
          icon: 'ios-megaphone',
        },
        {
          name: 'Show My Replies',
          description: 'See replies made by me',
          value: 'showMyReplies',
          icon: 'ios-megaphone',
        },
      ],
      renderItem: ({ item }) => (
        <ListItem
          maintext={item.name}
          subtext={item.description}
          leftSide={<Ionicons name={item.icon} size={30} />}
          rightSide={
            <Switch
              value={['favorites', 'bookmarks'].includes(timeline.timelineOption) || filters[item.value]}
              onValueChange={() => timeline.toggleFilter(item.value)}
              disabled={['favorites', 'bookmarks'].includes(timeline.timelineOption)}
            />
          }
        />
      ),
    }),
    [filters]
  );

  return (
    <View style={styles.container}>
      <SectionList
        sections={[timelineSection, timelineOptionsSettings]}
        renderSectionHeader={(section) => <SectionHeader {...section} />}
        keyExtractor={(item) => item.value}
        initialNumToRender={15}
        ListFooterComponent={
          <Text style={styles.version} colorName="inactive">
            v{Constants.manifest.version}
          </Text>
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  version: {
    textAlign: 'center',
  },
});

export default observer(TimelineOptionsScreen);
