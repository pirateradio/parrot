import * as React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { observer } from 'mobx-react-lite';
import unescape from 'lodash/unescape';

import useChats from '../hooks/useChats';
import ListItem from '../components/ListItem';
import PleromaText from '../components/PleromaText';
import Avatar from '../components/ConversationPanel/Avatar';
import { Text, View } from '../components/Themed';
import { ChatsTabParamList } from '../types';
import TimeAgo from '../components/TimeAgo';

const ChatsListScreen: React.FC<StackScreenProps<ChatsTabParamList, 'ChatsList'>> = ({ navigation }) => {
  const { chats } = useChats({});

  const handlePress = React.useCallback((chatId) => {
    navigation.navigate('Chat', { chatId });
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        ItemSeparatorComponent={() => <View style={styles.separator} colorName="border" />}
        renderItem={({ item: { account, last_message, updated_at } }) => (
          <ListItem
            onPress={() => handlePress(last_message?.chat_id)}
            leftSide={<Avatar account={account} />}
            center={
              <>
                <PleromaText
                  text={account.display_name}
                  emojis={account.emojis}
                  style={styles.displayName}
                  emojiSize={styles.displayName.fontSize}
                />
                <Text style={styles.subtext} colorName="secondary" numberOfLines={1}>
                  {unescape(last_message?.content || '') || (last_message?.attachment ? 'Media Attachment' : '')}
                </Text>
              </>
            }
            rightSide={<TimeAgo date={new Date(updated_at)} />}
          />
        )}
        data={chats.list}
        keyExtractor={({ id, last_message }) => `${id}-${last_message?.id || 'n/a'}`}
        onRefresh={() => chats.loadChatList()}
        refreshing={chats.refreshing}
        ListEmptyComponent={
          <Text style={{ textAlign: 'center' }} colorName="secondary">
            You have no chats
          </Text>
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  separator: {
    height: 1,
    width: '100%',
  },
  displayName: {
    fontWeight: 'bold',
    fontSize: 19,
  },
  subtext: {
    fontSize: 12,
    paddingTop: 5,
    fontStyle: 'italic',
  },
});

export default observer(ChatsListScreen);
