import * as React from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { StyleSheet, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { observer } from 'mobx-react-lite';

import SubheaderSearch from '../components/SubheaderSearch';

import { View, ActivityIndicator, Text } from '../components/Themed';
import { SearchTabParamList } from '../types';
import useSearch from '../hooks/useSearch';

const SearchScreen: React.FC<StackScreenProps<SearchTabParamList, 'Search'>> = () => {
  const {
    loading,
    search: { accounts, hashtags, statuses },
  } = useSearch();

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <SubheaderSearch />
        {loading ? (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <>
            <Text>{accounts.length} Accounts</Text>
            <Text>{hashtags.length} Hashtags</Text>
            <Text>{statuses.length} Statuses</Text>
          </>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  loadingContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
  },
});

export default observer(SearchScreen);
