import * as React from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { StyleSheet, FlatList } from 'react-native';
import { observer } from 'mobx-react-lite';

import { Text, View } from '../components/Themed';
import ListItem from '../components/ListItem';
import PleromaText from '../components/PleromaText';
import TimeAgo from '../components/TimeAgo';
import Avatar from '../components/ConversationPanel/Avatar';
import { NotificationsTabParamList } from '../types';
import useNotifications from '../hooks/useNotifications';

const NotificationsListScreen: React.FC<StackScreenProps<NotificationsTabParamList, 'NotificationsList'>> = () => {
  const { notifications } = useNotifications();

  return (
    <View style={styles.container}>
      <FlatList
        ItemSeparatorComponent={() => <View style={styles.separator} colorName="border" />}
        renderItem={({ item: { created_at, account, type } }) => (
          <ListItem
            leftSide={<Avatar account={account} />}
            center={
              <>
                <PleromaText
                  text={account.display_name}
                  emojis={account.emojis}
                  style={styles.displayName}
                  emojiSize={styles.displayName.fontSize}
                />
                <Text style={styles.subtext} colorName="secondary" numberOfLines={1}>
                  {type}
                </Text>
              </>
            }
            rightSide={<TimeAgo date={new Date(created_at)} />}
          />
        )}
        data={notifications.list}
        keyExtractor={({ id }) => id}
        onRefresh={() => notifications.loadNotificationList()}
        refreshing={notifications.refreshing}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  separator: {
    height: 1,
    width: '100%',
  },
  displayName: {
    fontWeight: 'bold',
    fontSize: 19,
  },
  subtext: {
    fontSize: 12,
    paddingTop: 5,
    fontStyle: 'italic',
  },
});

export default observer(NotificationsListScreen);
