import { StackScreenProps } from '@react-navigation/stack';
import { useScrollToTop } from '@react-navigation/native';
import { observer } from 'mobx-react-lite';
import * as React from 'react';
import { StyleSheet, FlatList } from 'react-native';

import ConversationPanel from '../components/ConversationPanel';
import { View } from '../components/Themed';

import useTimeline from '../hooks/useTimeline';
import { TimelineTabParamList } from '../types';

const TimelineScreen: React.FC<StackScreenProps<TimelineTabParamList, 'Timeline'>> = () => {
  const {
    timeline,
    timeline: { filteredConversations },
  } = useTimeline({
    onUpdate: (status) => timeline.addConversation(status),
  });

  const listRef = React.useRef<FlatList>(null);
  useScrollToTop(listRef);

  React.useEffect(() => {
    if (timeline.refreshing) listRef?.current?.scrollToOffset?.({ offset: 0 });
  }, [timeline.refreshing]);

  return (
    <View style={styles.container}>
      <FlatList
        ref={listRef}
        ItemSeparatorComponent={() => <View style={styles.separator} colorName="border" />}
        renderItem={({ item }) => <ConversationPanel convo={item} />}
        keyExtractor={({ id }) => id}
        data={filteredConversations}
        onRefresh={() => timeline.loadConversations()}
        refreshing={timeline.refreshing}
        onEndReached={() => timeline.loadMoreConversations()}
        onEndReachedThreshold={1.5}
        initialNumToRender={7}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    height: 8,
    width: '100%',
  },
});

export default observer(TimelineScreen);
