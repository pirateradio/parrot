import * as React from 'react';
import { Video } from 'expo-av';
import VPlayer from 'expo-video-player';
import { MediaAttachment } from '../types';
import { useThemeColor } from './Themed';

export type VideoPlayerProps = {
  video: MediaAttachment;
  contentWidth: number;
};

const VideoPlayer: React.FC<VideoPlayerProps> = ({ video, contentWidth }) => {
  const [height, setHeight] = React.useState(200);
  const green = useThemeColor({}, 'green');
  const componentIsMounted = React.useRef(true);

  React.useEffect(() => {
    componentIsMounted.current = true;

    return () => {
      componentIsMounted.current = false;
    };
  }, []);

  return (
    <VPlayer
      videoProps={{
        resizeMode: Video.RESIZE_MODE_CONTAIN,
        source: {
          uri: video.url,
        },
        onReadyForDisplay: ({ naturalSize: { height, width } }) => {
          if (componentIsMounted.current) setHeight(contentWidth * (height / width));
        },
      }}
      videoBackground="transparent"
      width={contentWidth}
      showControlsOnLoad={true}
      showFullscreenButton={false}
      height={height}
      sliderColor={green}
    />
  );
};

export default VideoPlayer;
