import * as React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { observer } from 'mobx-react-lite';
import { View, TouchableOpacity, Ionicons, useThemeColor } from './Themed';
import PressableIcon from './PressableIcon';
import useSearch from '../hooks/useSearch';

const SubheaderSearch: React.FC = () => {
  const [query, setQuery] = React.useState('');
  const primary = useThemeColor({}, 'primary');
  const inactive = useThemeColor({}, 'inactive');
  const inputRef = React.createRef<TextInput>();
  const { search, loading } = useSearch();

  return (
    <View colorName="base1" style={styles.container}>
      <TouchableOpacity
        colorName="base2"
        style={styles.inputContainer}
        onPress={() => inputRef.current?.focus()}
        disabled={loading}
      >
        <Ionicons colorName="secondary" name="ios-search-outline" size={18} />
        <TextInput
          ref={inputRef}
          style={[styles.input, { color: primary }]}
          placeholder="Search for users, statuses, hashtags..."
          placeholderTextColor={inactive}
          returnKeyType="search"
          value={query}
          editable={!loading}
          onChangeText={(newQuery) => setQuery(newQuery)}
          onSubmitEditing={() => search.runQuery(query)}
        />
        {query.trim().length > 2 && (
          <PressableIcon
            color="secondary"
            bgColorName="base2"
            size={18}
            name="ios-close-circle-outline"
            onPress={() => setQuery('')}
            disabled={loading}
            hitSlop={{ top: 3, bottom: 3, left: 3, right: 3 }}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  inputContainer: {
    alignItems: 'center',
    width: '100%',
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  input: {
    flex: 1,
    paddingLeft: 10,
    height: 40,
    fontSize: 16,
    margin: 0,
    width: '100%',
  },
});

export default observer(SubheaderSearch);
