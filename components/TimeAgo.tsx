import * as React from 'react';
import { StyleSheet, Platform } from 'react-native';
import { Text } from './Themed';

const MINUTE = 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;
const WEEK = DAY * 7;

const calcTimeAgo = (date: Date) => {
  const then = date.valueOf();
  const now = new Date().valueOf();

  const seconds = Math.round(Math.abs(now - then) / 1000);
  let period = seconds < MINUTE ? 1000 : seconds < HOUR ? 1000 * MINUTE : seconds < DAY ? 1000 * HOUR : 0;

  if (period > 60000) period = 60000; // cant set too long of a timeout

  const [value, unit] =
    seconds < MINUTE
      ? [Math.round(seconds), 's']
      : seconds < HOUR
      ? [Math.round(seconds / MINUTE), 'm']
      : seconds < DAY
      ? [Math.round(seconds / HOUR), 'h']
      : seconds < WEEK
      ? [Math.round(seconds / DAY), 'd']
      : [Math.round(seconds / WEEK), 'w'];

  return { period, value, unit };
};

export type TimeAgoProps = {
  date: Date;
  size?: number;
};

const TimeAgo: React.FC<TimeAgoProps> = ({ date, size = 14 }) => {
  let timeoutId: NodeJS.Timeout | null = null;
  const componentIsMounted = React.useRef(true);
  const [timeAgoString, setTimeAgoString] = React.useState(() => {
    const { value, unit } = calcTimeAgo(date);
    return `${value}${unit}`;
  });

  const tick = React.useCallback(() => {
    const { period, value, unit } = calcTimeAgo(date);
    const newTimeAgoString = `${value}${unit}`;
    if (newTimeAgoString !== timeAgoString && componentIsMounted.current) setTimeAgoString(newTimeAgoString);
    if (period) {
      if (timeoutId) clearTimeout(timeoutId);
      if (componentIsMounted.current) timeoutId = setTimeout(tick, period);
    }
  }, [date, timeAgoString]);

  React.useEffect(() => {
    tick();

    return () => {
      if (timeoutId) clearTimeout(timeoutId);
      componentIsMounted.current = false;
    };
  }, []);

  return (
    <Text style={[styles.text, { fontSize: size }]} colorName="secondary">
      {timeAgoString}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    marginTop: Platform.OS === 'android' ? -2 : 0,
  },
});

export default TimeAgo;
