import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text, useThemeColor } from './Themed';

type SectionHeaderProps = {
  section: {
    title: string;
  };
};

const SectionHeader: React.FC<SectionHeaderProps> = ({ section: { title } }) => {
  const borderBottomColor = useThemeColor({}, 'border');
  return (
    <View style={{ ...styles.container, borderBottomColor }} colorName="base2">
      <Text style={styles.content}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    justifyContent: 'center',
    height: 30,
    borderBottomWidth: 1,
  },
  content: {
    fontSize: 12,
    textTransform: 'uppercase',
  },
});

export default SectionHeader;
