import * as React from 'react';
import { Image, ImageProps, View } from 'react-native';
import * as FileSystem from 'expo-file-system';
import * as Sentry from 'sentry-expo';

type Override<T, U> = Omit<T, keyof U> & U;

type CachedImageProps = Override<
  ImageProps,
  { source: string | null | undefined; cacheKey?: string; forceHeight?: number }
>;

const CachedImage: React.FC<CachedImageProps> = ({ source = '', cacheKey, forceHeight = 0, style, ...props }) => {
  if (!source) return null;
  const [width, setWidth] = React.useState(0);

  const filesystemURI = React.useMemo(() => {
    if (cacheKey) return `${FileSystem.cacheDirectory}${cacheKey}`;
    const lastSlashIndex = source.lastIndexOf('/');
    if (lastSlashIndex === -1) return '';
    const genCacheKey = source.slice(lastSlashIndex + 1);
    return `${FileSystem.cacheDirectory}${genCacheKey}`;
  }, [source]);

  const [imgURI, setImgURI] = React.useState<string | null>(filesystemURI);

  const componentIsMounted = React.useRef(true);

  React.useEffect(() => {
    const loadImage = async (fileURI: string) => {
      try {
        // Use the cached image if it exists
        const metadata = await FileSystem.getInfoAsync(fileURI);
        if (!metadata.exists) {
          // download to cache
          if (componentIsMounted.current) {
            setImgURI(null);
            await FileSystem.downloadAsync(source, fileURI);
          }
          if (componentIsMounted.current) {
            setImgURI(fileURI);
          }
        }
        if (forceHeight) {
          Image.getSize(fileURI, (w, h) => {
            if (componentIsMounted.current) setWidth((w / h) * forceHeight);
          });
        }
      } catch (err) {
        Sentry.Native.captureException(err);
        setImgURI(source);
      }
    };

    if (filesystemURI) loadImage(filesystemURI);

    return () => {
      componentIsMounted.current = false;
    };
  }, [filesystemURI]);

  const computedStyle = React.useMemo(() => {
    if (!forceHeight || !width) return style;
    return [style, { height: forceHeight, width }];
  }, [width]);

  if (!imgURI) return <View {...props} />;
  return <Image style={computedStyle} {...props} source={{ uri: imgURI }} />;
};

export default CachedImage;
