import * as React from 'react';
import { Linking, View } from 'react-native';
import { useThemeColor } from './Themed';
import HTML, { extendDefaultRenderer, HTMLContentModel } from 'react-native-render-html';
import { Emoji, Status } from '../types';

export type PleromaHTMLProps = {
  content: Status['content'];
  emojis: Emoji[];
  contentWidth: number;
  baseStyle?: React.ComponentProps<typeof HTML>['baseStyle'];
};

const emojiRegex = /:[a-z0-9_-].*?:/gm;

const PleromaHTML: React.FC<PleromaHTMLProps> = ({ content = '', emojis = [], contentWidth, baseStyle = {} }) => {
  if (!content) return null;
  const linkColor = useThemeColor({}, 'green');
  const primaryColor = useThemeColor({}, 'primary');
  const parsedContent = React.useMemo(() => {
    let res = content;
    const emojiMatches = content.match(emojiRegex);
    if (emojiMatches) {
      emojiMatches.forEach((emojiShortcode) => {
        const matchingEmoji = emojis.find(
          ({ shortcode }) => shortcode === emojiShortcode.slice(1, emojiShortcode.length - 1)
        );
        if (matchingEmoji) {
          res = content.replace(
            emojiShortcode,
            `<imginline src="${matchingEmoji.static_url}" width="18" height="18"></imginline>`
          );
        }
      });
    }
    return res;
  }, [content]);

  return (
    <View style={{ marginBottom: 4 }}>
      <HTML
        contentWidth={contentWidth}
        source={{ html: parsedContent }}
        tagsStyles={{
          a: { textDecorationLine: 'none', color: linkColor },
        }}
        baseStyle={{ color: primaryColor, ...baseStyle }}
        onLinkPress={(_, href) => Linking.openURL(href)}
        renderers={{
          imginline: extendDefaultRenderer('img', { contentModel: HTMLContentModel.mixed }),
        }}
        defaultTextProps={{ selectable: true }}
        debug={false}
      />
    </View>
  );
};

export default PleromaHTML;
