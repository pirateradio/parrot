import * as React from 'react';
import * as Linking from 'expo-linking';
import { StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import { Text, useThemeColor } from './Themed';
import { ColorProps } from '../constants/Colors';
import CachedImage from './CachedImage';
import { Status } from '../types';

export type StatusCardProps = {
  card: Status['card'];
  containerColor?: keyof ColorProps;
};

const StatusCard: React.FC<StatusCardProps> = ({ card, containerColor = 'base2' }) => {
  if (!card) return null;

  const borderColor = useThemeColor({}, 'base2');
  const bgColor = useThemeColor({}, containerColor);
  return (
    <TouchableWithoutFeedback onPress={() => Linking.openURL(card.url)}>
      <View style={{ ...styles.lgContainer, borderColor, backgroundColor: bgColor }}>
        <CachedImage source={card.image} forceHeight={80} />
        <View style={styles.textContainer}>
          <Text colorName="green" style={styles.title} numberOfLines={1}>
            {card.title}
          </Text>
          <Text numberOfLines={2} colorName="secondary" style={{ flex: 1 }}>
            {card.description}
          </Text>
          <Text colorName="secondary" style={{ fontSize: 12 }}>
            {card.provider_name}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  lgContainer: {
    flexDirection: 'row',
    width: '100%',
    borderWidth: 1,
    borderRadius: 4,
    flex: 1,
    marginTop: 5,
    overflow: 'hidden',
    maxHeight: 80,
  },
  textContainer: {
    flex: 1,
    padding: 5,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default StatusCard;
