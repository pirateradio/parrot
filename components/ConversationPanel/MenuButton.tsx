import * as React from 'react';
import { useActionSheet } from '@expo/react-native-action-sheet';
import Toast from 'react-native-root-toast';
import Clipboard from 'expo-clipboard';
import { user } from '../../stores/user';
import PressableIcon from '../PressableIcon';

import { ConversationPanelProps } from '.';
import { timeline } from '../../stores/timeline';
import { HeaderHeightContext } from '@react-navigation/stack';

const MenuButton: React.FC<Pick<ConversationPanelProps, 'convo'>> = ({ convo }) => {
  const headerHeight = React.useContext(HeaderHeightContext);
  const { showActionSheetWithOptions } = useActionSheet();

  const handlePress = React.useCallback(() => {
    const options: string[] = ['Copy Link'];
    if (convo.visibility === 'public' && convo.account.id === user.id)
      options.push(convo.pinned ? 'Unpin from Profile' : 'Pin to Profile');
    options.push(convo.muted ? 'Unmute Post' : 'Mute Post');
    if (convo.account.id === user.id) options.push('Delete Post');
    else options.push('Report');
    options.push('Cancel');
    const cancelButtonIndex = options.indexOf('Cancel');
    const destructiveButtonIndex = options.indexOf('Delete Post');
    showActionSheetWithOptions({ options, cancelButtonIndex, destructiveButtonIndex }, (buttonIndex) => {
      switch (options[buttonIndex]) {
        case 'Mute Post':
        case 'Unmute Post':
          timeline.setMute(convo.id, !convo.muted);
          break;
        case 'Delete Post':
          timeline.deleteStatus(convo.id);
          break;
        case 'Pin to Profile':
        case 'Unpin from Profile':
          timeline.setPin(convo.id, !convo.pinned);
          break;
        case 'Report':
          Toast.show('Not Implemented Yet', {
            position: headerHeight ? headerHeight + 10 : 100,
          });
          break;
        case 'Copy Link':
          Clipboard.setString(convo.url);
          Toast.show('Link Copied to Clipboard', {
            position: headerHeight ? headerHeight + 10 : 100,
          });
          break;
        default:
          break;
      }
    });
  }, [convo]);

  return (
    <PressableIcon
      size={17}
      style={{ marginLeft: 8 }}
      name="ios-ellipsis-horizontal"
      hitSlop={{ top: 3, left: 3, right: 3, bottom: 3 }}
      onPress={handlePress}
      color="secondary"
    />
  );
};

export default MenuButton;
