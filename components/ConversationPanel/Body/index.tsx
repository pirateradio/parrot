import * as React from 'react';
import { StyleSheet, useWindowDimensions } from 'react-native';
import { View } from '../../Themed';
import PleromaHTML from '../../PleromaHTML';
import { ConversationPanelProps } from '../';
import { MediaAttachment } from '../../../types';
import AudioPlayer from '../../AudioPlayer';
import VideoPlayer from '../../VideoPlayer';
import ImageScrollView from '../../ImageScrollView';
import StatusCard from '../../StatusCard';

const Body: React.FC<Pick<ConversationPanelProps, 'convo'>> = ({
  convo: { emojis, content = '', media_attachments, card, sensitive },
}) => {
  const contentWidth = useWindowDimensions().width - 36;

  const [audios, images, videos] = React.useMemo(() => {
    const a: MediaAttachment[] = [];
    const i: MediaAttachment[] = [];
    const v: MediaAttachment[] = [];
    media_attachments.forEach((attachment) => {
      if (attachment.type === 'audio') a.push(attachment);
      else if (attachment.type === 'image') i.push(attachment);
      else if (attachment.type === 'video') v.push(attachment);
    });
    return [a, i, v];
  }, []);

  return (
    <View style={styles.container} colorName="base1">
      <PleromaHTML content={content} emojis={emojis} contentWidth={contentWidth} baseStyle={styles.note} />
      <ImageScrollView images={images} contentWidth={contentWidth} sensitive={sensitive} />
      {videos.map((video) => (
        <VideoPlayer key={video.id} video={video} contentWidth={contentWidth} />
      ))}
      {audios.map((audio) => (
        <AudioPlayer key={audio.id} audio={audio} />
      ))}
      <StatusCard card={card} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
    marginTop: 5,
    padding: 8,
    borderRadius: 7,
  },
  note: {
    fontSize: 16,
    fontWeight: '500',
  },
});

export default Body;
