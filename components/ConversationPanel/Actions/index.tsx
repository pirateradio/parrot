import * as React from 'react';
import { StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { View, Ionicons, TouchableOpacity } from '../../Themed';
import LikeButton from './LikeButton';
import BoostButton from './BoostButton';
import BookmarkButton from './BookmarkButton';
import EmojiReactButton from './EmojiReactButton';

import { ConversationPanelProps } from '../';

const Actions: React.FC<Pick<ConversationPanelProps, 'convo'>> = ({
  convo: {
    favourited,
    favourites_count,
    reblogged,
    reblogs_count,
    bookmarked,
    id: convoId,
    pleroma: { emoji_reactions },
  },
}) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <LikeButton
        statusId={convoId}
        style={styles.actionItem}
        favourited={favourited}
        favourites_count={favourites_count}
      />
      <BoostButton statusId={convoId} style={styles.actionItem} reblogged={reblogged} reblogs_count={reblogs_count} />
      <EmojiReactButton statusId={convoId} style={styles.actionItem} emojiReactions={emoji_reactions} />
      <BookmarkButton statusId={convoId} style={styles.actionItem} bookmarked={bookmarked} />
      <TouchableOpacity
        style={styles.actionItem}
        colorName="transparent"
        onPress={() => navigation.navigate('Conversation', { convoId, focusConvoId: convoId })}
      >
        <Ionicons size={20} name="ios-chatbox-outline" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
  },
  actionItem: {
    flexDirection: 'row',
    flex: 1,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Actions;
