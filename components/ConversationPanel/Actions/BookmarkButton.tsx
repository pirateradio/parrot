import * as React from 'react';
import { ViewProps, TouchableOpacity } from 'react-native';
import { Ionicons } from '../../Themed';
import { Status } from '../../../types';
import { timeline } from '../../../stores/timeline';

export type BookmarkButtonProps = {
  style?: ViewProps['style'];
  bookmarked: Status['bookmarked'];
  statusId: Status['id'];
};

const BookmarkButton: React.FC<BookmarkButtonProps> = ({ style = {}, bookmarked, statusId }) => {
  const [localBook, setLocalBook] = React.useState(bookmarked);

  const handlePress = React.useCallback(() => {
    setLocalBook((prevBookmark) => {
      timeline.setBookmark(statusId, !prevBookmark);
      return !prevBookmark;
    });
  }, []);

  return (
    <TouchableOpacity style={style} onPress={handlePress}>
      <Ionicons
        size={20}
        name={localBook ? 'ios-bookmark' : 'ios-bookmark-outline'}
        colorName={localBook ? 'primary' : 'secondary'}
      />
    </TouchableOpacity>
  );
};

export default BookmarkButton;
