import * as React from 'react';
import { ViewProps, TouchableOpacity } from 'react-native';
import { Ionicons, Text } from '../../Themed';
import { Status } from '../../../types';
import { timeline } from '../../../stores/timeline';

export type LikeButtonProps = {
  style?: ViewProps['style'];
  favourited: Status['favourited'];
  favourites_count: Status['favourites_count'];
  statusId: Status['id'];
};

const LikeButton: React.FC<LikeButtonProps> = ({ style = {}, favourited, favourites_count, statusId }) => {
  // instead of forcing a re-render of the entire tree, we'll just flip the variable locally for the visual
  const [localFave, setLocalFave] = React.useState(favourited);
  const [localFaveCount, setLocalFaveCount] = React.useState(favourites_count);

  const handlePress = React.useCallback(() => {
    setLocalFave((prevFave) => {
      setLocalFaveCount((prevCount) => {
        if (prevFave) return prevCount - 1;
        return prevCount + 1;
      });
      timeline.setFavorite(statusId, !prevFave);
      return !prevFave;
    });
  }, []);

  return (
    <TouchableOpacity style={style} onPress={handlePress}>
      <Ionicons
        size={20}
        name={localFave ? 'ios-heart' : 'ios-heart-outline'}
        colorName={localFave ? 'primary' : 'secondary'}
      />
      <Text colorName={localFave ? 'primary' : 'secondary'}>{localFaveCount}</Text>
    </TouchableOpacity>
  );
};

export default LikeButton;
