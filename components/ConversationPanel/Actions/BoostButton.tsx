import * as React from 'react';
import { ViewProps, TouchableOpacity } from 'react-native';
import { HeaderHeightContext } from '@react-navigation/stack';
import Toast from 'react-native-root-toast';
import * as Haptics from 'expo-haptics';
import { Ionicons, Text } from '../../Themed';
import { Status } from '../../../types';
import { timeline } from '../../../stores/timeline';

export type BoostButtonProps = {
  style?: ViewProps['style'];
  reblogged: Status['reblogged'];
  reblogs_count: Status['reblogs_count'];
  statusId: Status['id'];
};

const BoostButton: React.FC<BoostButtonProps> = ({ style = {}, reblogged, reblogs_count, statusId }) => {
  const headerHeight = React.useContext(HeaderHeightContext);
  // instead of forcing a re-render of the entire tree, we'll just flip the variable locally for the visual
  const [localBoost, setLocalBoost] = React.useState(reblogged);
  const [localBoostCount, setLocalBoostCount] = React.useState(reblogs_count);

  const handlePress = React.useCallback(() => {
    Toast.show('Long Press to Boost', {
      position: headerHeight ? headerHeight + 10 : 100,
    });
  }, [headerHeight]);

  const handleLongPress = React.useCallback(() => {
    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
    timeline.setBoost(statusId, !localBoost);
    setLocalBoost(!localBoost);
    setLocalBoostCount(localBoost ? localBoostCount - 1 : localBoostCount + 1);
  }, [localBoost]);

  return (
    <TouchableOpacity style={style} onPress={handlePress} onLongPress={handleLongPress}>
      <Ionicons
        size={20}
        name={localBoost ? 'ios-rocket' : 'ios-rocket-outline'}
        colorName={localBoost ? 'primary' : 'secondary'}
      />
      <Text colorName={localBoost ? 'primary' : 'secondary'}>{localBoostCount}</Text>
    </TouchableOpacity>
  );
};

export default BoostButton;
