import * as React from 'react';
import { ViewProps, TouchableOpacity, Modal } from 'react-native';
import { useModal } from 'react-native-use-modal-hooks';
import { HeaderHeightContext } from '@react-navigation/stack';
import * as Haptics from 'expo-haptics';
import EmojiSelector, { Categories } from '@pirateradio/react-native-emoji-selector';
import Toast from 'react-native-root-toast';
import { timeline } from '../../../stores/timeline';
import RoundButton from '../../RoundButton';
import { Ionicons, Text, SafeAreaView, useThemeColor } from '../../Themed';
import { ConversationID, Status } from '../../../types';

export type EmojiReactButtonProps = {
  statusId: ConversationID;
  style?: ViewProps['style'];
  emojiReactions?: Status['pleroma']['emoji_reactions'];
};

const EmojiReactButton: React.FC<EmojiReactButtonProps> = ({ statusId, style = {}, emojiReactions = [] }) => {
  const headerHeight = React.useContext(HeaderHeightContext);
  const borderColor = useThemeColor({}, 'border');
  const green = useThemeColor({}, 'green');
  const myReaction = React.useMemo(() => emojiReactions.find(({ me }) => me), []);
  const [localReactEmoji, setLocalReactEmoji] = React.useState(myReaction?.name || '');

  const [showSelector, hideSelector] = useModal(
    () => (
      <Modal transparent animationType="slide" onRequestClose={hideSelector}>
        <SafeAreaView style={{ flex: 1, paddingTop: 20 }}>
          <EmojiSelector
            onEmojiSelected={async (emoji) => {
              if (localReactEmoji) {
                try {
                  await timeline.removeEmojiReaction(statusId, localReactEmoji);
                  setLocalReactEmoji('');
                } catch (err) {
                  //
                }
              }
              try {
                await timeline.setEmojiReaction(statusId, emoji);
                setLocalReactEmoji(emoji);
              } catch (err) {
                Toast.show(err.message, {
                  position: headerHeight ? headerHeight + 10 : 100,
                });
              }
              hideSelector();
            }}
            inactiveTheme={borderColor}
            theme={green}
            showSectionTitles={false}
            showHistory
            category={Categories.history}
          />
          {localReactEmoji ? (
            <RoundButton
              style={{ margin: 10 }}
              text="Remove"
              bgColorName="red"
              onPress={() => {
                timeline.removeEmojiReaction(statusId, localReactEmoji);
                setLocalReactEmoji('');
                hideSelector();
              }}
            />
          ) : null}
          <RoundButton style={{ margin: 10 }} text="Cancel" onPress={hideSelector} />
        </SafeAreaView>
      </Modal>
    ),
    [localReactEmoji]
  );

  const handlePress = React.useCallback(() => {
    Toast.show('Long Press to React', {
      position: headerHeight ? headerHeight + 10 : 100,
    });
  }, []);

  const handleLongPress = React.useCallback(() => {
    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
    showSelector();
  }, []);

  return (
    <TouchableOpacity style={style} onPress={handlePress} onLongPress={handleLongPress}>
      {localReactEmoji ? (
        <Text key={localReactEmoji} style={{ fontSize: 20 }}>
          {localReactEmoji}
        </Text>
      ) : (
        <Ionicons size={20} name="ios-happy-outline" colorName="secondary" />
      )}
    </TouchableOpacity>
  );
};

export default EmojiReactButton;
