import * as React from 'react';
import { StyleSheet } from 'react-native';

import { Status } from '../../types';
import { View } from '../Themed';
import Header from './Header';
import SuperHeader from './SuperHeader';
import Body from './Body';
import Actions from './Actions';
import Footer from './Footer';

/**
 * This component has a prop that is being passed to all of it's children, therefore in the children's prop definitions
 * you should point to this definition as a reference. The benefit of this is that if the parent's definition changes,
 * the children will automatically know of it. This can be done using the 'extends' keyword or by creating a new
 * property name and assigning it the desired definition.
 *
 * Example 1:
 * export interface ChildProps extends Pick<ParentProps, 'desiredProp1' | 'desiredProp2'> {
 *   childProp1: string;
 *   childProp2: number;
 * }
 *
 * Example 2 (Useful for renaming or modifying a prop):
 * export interface ChildProps {
 *   desiredProp1: ParentProp['desiredProp1'];
 *   renamedProp2?: ParentProp['desiredProp2'] | string;
 *   childProp1: string;
 *   childProp2: number;
 * }
 */

export interface ConversationPanelProps {
  convo: Status;
}

const ConversationPanel: React.FC<ConversationPanelProps> = ({ convo }) => {
  return (
    <View style={styles.container}>
      <SuperHeader convo={convo} />
      <Header convo={convo.reblog || convo} />
      <Body convo={convo.reblog || convo} />
      <Actions convo={convo.reblog || convo} />
      <Footer convo={convo.reblog || convo} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    minHeight: 60,
  },
});

export default React.memo(ConversationPanel);
