import * as React from 'react';
import { StyleSheet, Platform } from 'react-native';

import { ConversationPanelProps } from '.';
import { Text, View } from '../Themed';
import MenuButton from './MenuButton';
import PleromaText from '../PleromaText';
import TimeAgo from '../TimeAgo';
import Avatar from './Avatar';
import Visibility from './Visibility';
import PressableIcon from '../PressableIcon';

const Header: React.FC<Pick<ConversationPanelProps, 'convo'>> = ({ convo }) => {
  const {
    account,
    account: { acct, display_name, emojis },
    created_at,
    visibility,
  } = convo;

  return (
    <View style={styles.container}>
      <Avatar account={account} />
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={styles.nameContainer}>
          <PleromaText
            style={styles.displayName}
            emojiSize={styles.displayName.fontSize}
            emojis={emojis}
            text={display_name}
            numberOfLines={1}
          />
          <Text style={styles.username} colorName="secondary" numberOfLines={1}>
            {acct}
          </Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', paddingTop: 3 }}>
        <Visibility visibility={visibility} />
        <TimeAgo date={new Date(created_at)} />
        <MenuButton convo={convo} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 30,
    margin: 10,
    marginHorizontal: 12,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  nameContainer: {
    marginLeft: 5,
  },
  displayName: {
    fontWeight: 'bold',
    fontSize: 19,
    marginTop: Platform.OS === 'android' ? -3 : 0,
  },
  username: {
    fontSize: 13,
    marginTop: Platform.OS === 'android' ? -4 : 0,
  },
});

export default Header;
