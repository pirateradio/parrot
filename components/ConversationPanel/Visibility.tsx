import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Ionicons } from '../Themed';
import { Status } from '../../types';

type VisibilityProps = {
  visibility: Status['visibility'];
};

const Visibility: React.FC<VisibilityProps> = ({ visibility }) => {
  const icon = React.useMemo(() => {
    switch (visibility) {
      case 'private':
      case 'direct':
        return 'ios-mail';
      case 'public':
        return 'md-globe';
      case 'unlisted':
        return 'ios-close-circle';
    }
  }, [visibility]);

  return <Ionicons name={icon} colorName="secondary" style={styles.icon} />;
};

const styles = StyleSheet.create({
  icon: {
    marginRight: 3,
    paddingTop: 2,
  },
});

export default Visibility;
