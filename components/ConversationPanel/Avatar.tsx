import * as React from 'react';
import { StyleSheet } from 'react-native';
import CachedImage from '../CachedImage';
import { UserAccount } from '../../types';

type AvatarProps = {
  account: UserAccount;
};

const Avatar: React.FC<AvatarProps> = ({ account }) => (
  <CachedImage source={account.avatar_static} style={styles.avatar} />
);

const styles = StyleSheet.create({
  avatar: {
    height: 32,
    width: 32,
    borderRadius: 3,
    marginTop: 2,
  },
});

export default Avatar;
