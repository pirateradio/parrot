import * as React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text, Ionicons } from '../Themed';

import { ConversationPanelProps } from '.';
import PleromaText from '../PleromaText';

const SuperHeader: React.FC<Pick<ConversationPanelProps, 'convo'>> = ({ convo }) => {
  if (convo.reblog) {
    return (
      <View style={styles.container}>
        <PleromaText
          style={{ fontWeight: 'bold' }}
          textColor="inactive"
          text={`${convo.account.display_name} `}
          emojis={convo.account.emojis}
        />
        <Ionicons size={16} colorName="inactive" name="ios-rocket" />
      </View>
    );
  }

  return null;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: 5,
    marginBottom: -5,
    marginLeft: 47,
  },
});

export default SuperHeader;
