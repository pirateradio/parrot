import * as React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text, TouchableOpacity, useThemeColor } from './Themed';

type ListItemProps = {
  maintext?: React.ReactNode;
  subtext?: React.ReactNode;
  onPress?: () => void;
  onLongPress?: () => void;
  rightSide?: React.ReactNode;
  leftSide?: React.ReactNode;
  center?: React.ReactNode;
  selected?: boolean;
};

const ListItem: React.FC<ListItemProps> = React.memo(
  ({ maintext, subtext, onPress, onLongPress, rightSide, leftSide, center, selected = false }) => {
    const Wrapper = React.useMemo(() => (onPress || onLongPress ? TouchableOpacity : View), [onPress, onLongPress]);
    const borderColor = useThemeColor({}, 'border');
    const wrapperStyles = React.useMemo(() => [styles.item, { borderColor }], [borderColor]);
    const bgColor = selected ? 'base1' : 'base0';

    return (
      <Wrapper style={wrapperStyles} colorName={bgColor} onPress={onPress} onLongPress={onLongPress}>
        {leftSide ? (
          <View style={styles.left} colorName={bgColor}>
            {leftSide}
          </View>
        ) : null}
        <View style={styles.center} colorName={bgColor}>
          {center ? (
            center
          ) : (
            <>
              <Text style={{ fontWeight: 'bold' }}>{maintext}</Text>
              {subtext ? (
                <Text style={styles.subtext} colorName="secondary" numberOfLines={1}>
                  {subtext}
                </Text>
              ) : null}
            </>
          )}
        </View>
        {rightSide ? (
          <View style={styles.right} colorName={bgColor}>
            {rightSide}
          </View>
        ) : null}
      </Wrapper>
    );
  }
);

const styles = StyleSheet.create({
  item: {
    height: 50,
    width: '100%',
    borderBottomWidth: 1,
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 5,
  },
  left: {
    marginRight: 10,
    justifyContent: 'center',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
  },
  right: {
    marginLeft: 10,
    justifyContent: 'center',
  },
  subtext: {
    fontSize: 12,
    paddingTop: 5,
  },
});

export default ListItem;
