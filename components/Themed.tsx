import * as React from 'react';
import {
  Text as DefaultText,
  View as DefaultView,
  TouchableOpacity as DefaultTouchableOpacity,
  Switch as DefaultSwitch,
  TextInput as DefaultTextInput,
  ActivityIndicator as DefaultActivityIndicator,
} from 'react-native';
import { SafeAreaView as DefaultSafeAreaView } from 'react-native-safe-area-context';
import { Ionicons as DefaultIonicons, FontAwesome5 as DefaultFontAwesome5 } from '@expo/vector-icons';

import colors, { ColorKeys, ColorProps } from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';

export type UseThemeColorProps = Partial<Record<ColorKeys, string>>;

export const useThemeColor = (props: UseThemeColorProps, colorName: keyof ColorProps): string => {
  const theme = useColorScheme();
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  } else {
    return colors[theme][colorName];
  }
};

export type ThemeProps = Partial<Record<ColorKeys, string>>;

export type TextProps = ThemeProps & DefaultText['props'] & { colorName?: keyof ColorProps };
export type ViewProps = ThemeProps & DefaultView['props'] & { colorName?: keyof ColorProps };
export type SafeAreaViewProps = ThemeProps &
  React.ComponentProps<typeof DefaultSafeAreaView> & { colorName?: keyof ColorProps };
export type TouchableOpacityProps = ThemeProps & DefaultTouchableOpacity['props'] & { colorName?: keyof ColorProps };
export type IoniconsProps = ThemeProps &
  React.ComponentProps<typeof DefaultIonicons> & { colorName?: keyof ColorProps };
export type FontAwesome5Props = ThemeProps &
  React.ComponentProps<typeof DefaultFontAwesome5> & { colorName?: keyof ColorProps };
export type SwitchProps = ThemeProps &
  DefaultSwitch['props'] & { offColorName?: keyof ColorProps; onColorName?: keyof ColorProps };
export type TextInputProps = ThemeProps &
  DefaultTextInput['props'] & { colorName?: keyof ColorProps; placeholderColorName?: keyof ColorProps };
export type ActivityIndicatorProps = ThemeProps & DefaultActivityIndicator['props'] & { colorName?: keyof ColorProps };

export const Text: React.FC<TextProps> = ({ style, light, dark, colorName = 'primary', ...otherProps }) => {
  const color = useThemeColor({ light, dark }, colorName);

  return <DefaultText style={[{ color }, style]} {...otherProps} />;
};

export const View: React.FC<ViewProps> = ({ style, light, dark, colorName = 'base0', ...otherProps }) => {
  const backgroundColor = useThemeColor({ light, dark }, colorName);

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
};

export const SafeAreaView: React.FC<SafeAreaViewProps> = ({
  style,
  light,
  dark,
  colorName = 'base0',
  ...otherProps
}) => {
  const backgroundColor = useThemeColor({ light, dark }, colorName);

  return <DefaultSafeAreaView style={[{ backgroundColor }, style]} {...otherProps} />;
};

export const TouchableOpacity: React.FC<TouchableOpacityProps> = ({
  style,
  light,
  dark,
  colorName = 'base0',
  ...otherProps
}) => {
  const backgroundColor = useThemeColor({ light, dark }, colorName);

  return <DefaultTouchableOpacity style={[{ backgroundColor }, style]} {...otherProps} />;
};

export const Ionicons: React.FC<IoniconsProps> = ({ light, dark, colorName = 'primary', ...otherProps }) => {
  const color = useThemeColor({ light, dark }, colorName);

  return <DefaultIonicons color={color} {...otherProps} />;
};

export const FontAwesome5: React.FC<FontAwesome5Props> = ({ light, dark, colorName = 'primary', ...otherProps }) => {
  const color = useThemeColor({ light, dark }, colorName);

  return <DefaultFontAwesome5 color={color} {...otherProps} />;
};

export const Switch: React.FC<SwitchProps> = ({
  light,
  dark,
  offColorName = 'base3',
  onColorName = 'green',
  ...otherProps
}) => {
  const offColor = useThemeColor({ light, dark }, offColorName);
  const onColor = useThemeColor({ light, dark }, onColorName);

  return <DefaultSwitch trackColor={{ true: onColor, false: offColor }} {...otherProps} />;
};

export const TextInput: React.FC<TextInputProps> = ({
  light,
  dark,
  colorName = 'primary',
  placeholderColorName = 'secondary',
  style = {},
  ...otherProps
}) => {
  const fontColor = useThemeColor({ light, dark }, colorName);
  const placeholderColor = useThemeColor({ light, dark }, placeholderColorName);

  return (
    <DefaultTextInput placeholderTextColor={placeholderColor} style={[style, { color: fontColor }]} {...otherProps} />
  );
};

export const ActivityIndicator: React.FC<ActivityIndicatorProps> = ({
  light,
  dark,
  colorName = 'primary',
  ...otherProps
}) => {
  const color = useThemeColor({ light, dark }, colorName);

  return <DefaultActivityIndicator color={color} {...otherProps} />;
};
