import * as React from 'react';
import { TouchableOpacity, StyleSheet, ViewProps } from 'react-native';

import { useThemeColor, Text } from './Themed';
import { ColorProps } from '../constants/Colors';

export type RoundButtonProps = TouchableOpacity['props'] & {
  fontColorName?: keyof ColorProps;
  bgColorName?: keyof ColorProps;
  text: string;
  style?: ViewProps['style'];
};

const RoundButton: React.FC<RoundButtonProps> = ({
  fontColorName = 'white',
  bgColorName = 'green',
  text,
  disabled,
  style,
  ...otherProps
}) => {
  const bgColor = useThemeColor({}, bgColorName);

  return (
    <TouchableOpacity
      style={[styles.container, { backgroundColor: bgColor }, style]}
      disabled={disabled}
      {...otherProps}
    >
      <Text colorName={disabled ? 'inactive' : fontColorName} style={styles.label}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    minHeight: 40,
    borderRadius: 100,
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  label: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default RoundButton;
