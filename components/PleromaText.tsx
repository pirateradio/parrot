import * as React from 'react';
import ParsedText from '@pirateradio/react-native-parsed-text';
import * as Linking from 'expo-linking';
import { Emoji, Mention } from '../types';
import { TextProps, useThemeColor } from './Themed';
import CachedImage from './CachedImage';
import { ColorProps } from '../constants/Colors';

export type PleromaTextProps = {
  text?: string;
  emojis: Emoji[];
  mentions?: Mention[];
  style?: TextProps['style'];
  emojiSize?: number;
  linkStyle?: TextProps['style'];
  textColor?: keyof ColorProps;
  numberOfLines?: TextProps['numberOfLines'];
};

const onUrlPress = (url: string) => {
  // When someone sends a message that includes a website address beginning with "www." (omitting the scheme),
  // react-native-parsed-text recognizes it as a valid url, but Linking fails to open due to the missing scheme.
  if (/^www\./i.test(url)) {
    onUrlPress(`http://${url}`);
  } else {
    Linking.canOpenURL(url).then((supported) => {
      if (!supported) {
        console.error('No handler for URL:', url);
      } else {
        Linking.openURL(url);
      }
    });
  }
};

const PleromaText: React.FC<PleromaTextProps> = ({
  text = '',
  emojis,
  mentions = [],
  style = {},
  emojiSize = 12,
  linkStyle = {},
  textColor = 'primary',
  numberOfLines,
}) => {
  const fontColor = useThemeColor({}, textColor);
  const linkColor = useThemeColor({}, 'green');

  const renderEmoji = React.useMemo(
    () => (emojiString: string) => {
      const matchingEmoji = emojis.find(({ shortcode }) => shortcode === emojiString.slice(1, emojiString.length - 1));
      if (matchingEmoji)
        return (
          <CachedImage
            style={{ height: emojiSize, width: emojiSize, marginBottom: -2 }}
            source={matchingEmoji.static_url}
          />
        );
      return emojiString;
    },
    [emojis]
  );

  const handleMentionPress = React.useCallback((matchingName: string) => {
    const matchingMention = mentions.find(
      ({ username }) => username.toUpperCase() === matchingName.slice(1).toUpperCase()
    );
    console.log(matchingMention);
    // TODO
  }, []);

  return (
    <ParsedText
      style={[{ color: fontColor }, style]}
      parse={[
        {
          pattern: /:[a-z0-9_-].*?:/gm,
          renderText: renderEmoji,
        },
        {
          pattern: /(?:^|[^a-zA-Z0-9_＠!@#$%&*])(?:(?:@|＠)(?!\/))([a-zA-Z0-9/_]{1,101})(?:\b(?!@|＠)|$)/gm,
          style: [{ color: linkColor }, linkStyle],
          onPress: handleMentionPress,
        },
        {
          type: 'url',
          style: [{ color: linkColor }, linkStyle],
          onPress: onUrlPress,
        },
      ]}
      numberOfLines={numberOfLines}
    >
      {text}
    </ParsedText>
  );
};

export default PleromaText;
