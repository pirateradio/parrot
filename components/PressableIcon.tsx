import * as React from 'react';
import { TouchableHighlightProps } from 'react-native';
import { TouchableOpacity, Ionicons, IoniconsProps } from '../components/Themed';
import { ColorProps } from '../constants/Colors';

interface PressableIconProps extends TouchableHighlightProps {
  name: IoniconsProps['name'];
  color?: keyof ColorProps;
  size?: number;
  bgColorName?: keyof ColorProps;
}

const PressableIcon: React.FC<PressableIconProps> = ({
  name,
  color = 'primary',
  size = 30,
  bgColorName,
  ...touchableProps
}) => (
  <TouchableOpacity colorName={bgColorName} {...touchableProps}>
    <Ionicons name={name} colorName={color} size={size} />
  </TouchableOpacity>
);

export default PressableIcon;
