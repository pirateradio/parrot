import * as React from 'react';
import { FlatList, TouchableWithoutFeedback, SafeAreaView } from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import ImageView from 'react-native-image-viewing';
import * as Sharing from 'expo-sharing';
import * as FileSystem from 'expo-file-system';
import * as Sentry from 'sentry-expo';
import CachedImage from './CachedImage';
import { MediaAttachment } from '../types';
import { Text } from './Themed';

export type ImageScrollViewProps = {
  images: MediaAttachment[];
  scrollHeight?: number;
  contentWidth: number;
  sensitive?: boolean;
};

const ImageScrollView: React.FC<ImageScrollViewProps> = ({
  images,
  scrollHeight = 140,
  contentWidth,
  sensitive = false,
}) => {
  if (!images.length) return null;

  const imageUris = React.useMemo(() => images.map(({ url }) => ({ uri: url })), [images]);
  const [imageIndex, setImageIndex] = React.useState(0);
  const [modalVisible, setModalVisible] = React.useState(false);

  const Footer = React.useMemo<React.ComponentProps<typeof ImageView>['FooterComponent']>(
    () => ({ imageIndex: iIndex }) => (
      <SafeAreaView>
        <Text style={{ textAlign: 'center' }} colorName="light">
          {iIndex + 1}/{images.length}: {images[iIndex].description || 'No description available'}
        </Text>
      </SafeAreaView>
    ),
    []
  );

  const handleLongPress = React.useCallback(async (image: MediaAttachment) => {
    try {
      const canShare = await Sharing.isAvailableAsync();
      if (canShare) {
        const lastSlashIndex = image.url.lastIndexOf('/');
        const key = image.url.slice(lastSlashIndex + 1);
        const fileUri = `${FileSystem.cacheDirectory}${key}`;
        await FileSystem.downloadAsync(image.url, fileUri);
        Sharing.shareAsync(fileUri, {
          mimeType: image.pleroma.mime_type,
          UTI: image.pleroma.mime_type,
          dialogTitle: image.description || '',
        });
      }
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }, []);

  return (
    <>
      {images.length === 1 ? (
        <TouchableWithoutFeedback onPress={() => setModalVisible(true)} onLongPress={() => handleLongPress(images[0])}>
          <AutoHeightImage
            style={{ borderRadius: 5 }}
            source={{ uri: images[0].preview_url }}
            width={contentWidth}
            maxHeight={300}
            blurRadius={sensitive ? 15 : 0}
          />
        </TouchableWithoutFeedback>
      ) : (
        <FlatList
          style={{ backgroundColor: '#000', height: scrollHeight, borderRadius: 5 }}
          horizontal
          bounces={false}
          data={images}
          renderItem={({ item, index }) => (
            <TouchableWithoutFeedback
              onPress={() => {
                setImageIndex(index);
                setModalVisible(true);
              }}
              onLongPress={() => handleLongPress(item)}
            >
              <CachedImage source={item.preview_url} blurRadius={sensitive ? 15 : 0} forceHeight={scrollHeight} />
            </TouchableWithoutFeedback>
          )}
        />
      )}
      <ImageView
        visible={modalVisible}
        images={imageUris}
        imageIndex={imageIndex}
        onRequestClose={() => setModalVisible(false)}
        FooterComponent={Footer}
      />
    </>
  );
};

export default ImageScrollView;
