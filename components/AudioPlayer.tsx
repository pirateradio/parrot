import * as React from 'react';
import { StyleSheet } from 'react-native';
import Slider from '@react-native-community/slider';
import { useThemeColor, View } from './Themed';
import PressableIcon from './PressableIcon';
import { MediaAttachment } from '../types';
import useAudioPlayback from '../hooks/useAudioPlayback';

export type AudioPlayerProps = {
  audio: MediaAttachment;
};

const AudioPlayer: React.FC<AudioPlayerProps> = ({ audio }) => {
  const { isLoading, isPlaying, isBuffering, pause, play, durationMs, positionMs, setPositionMs } = useAudioPlayback({
    url: audio.url,
  });
  const green = useThemeColor({}, 'green');
  const primary = useThemeColor({}, 'primary');
  const inverted = useThemeColor({}, 'inverted');
  const inactive = useThemeColor({}, 'inactive');

  // the slider jumps around while sliding unless we freeze the value
  const [frozenPosition, setFrozenPosition] = React.useState(0);

  const icon = React.useMemo(() => {
    if (isPlaying && !isBuffering) return 'ios-pause';
    if (isBuffering) return 'ios-cloud-download';
    return 'ios-play';
  }, [isPlaying]);

  return (
    <View style={styles.container} colorName="base2">
      <PressableIcon
        name={icon}
        color={isLoading ? 'inactive' : 'primary'}
        disabled={isLoading}
        onPress={isPlaying ? pause : play}
        bgColorName="transparent"
        hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}
      />
      <Slider
        style={{ flex: 1 }}
        minimumValue={0}
        maximumValue={durationMs || 1}
        value={durationMs ? frozenPosition || positionMs : 0}
        minimumTrackTintColor={green}
        maximumTrackTintColor={inverted}
        disabled={isLoading || !durationMs}
        thumbTintColor={isLoading ? inactive : primary}
        onSlidingComplete={(newPositionMs) => {
          setPositionMs(newPositionMs);
          setFrozenPosition(0);
        }}
        onSlidingStart={() => {
          setFrozenPosition(positionMs);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 7,
    paddingHorizontal: 5,
    alignItems: 'center',
    height: 35,
  },
});

export default AudioPlayer;
