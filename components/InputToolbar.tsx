import * as React from 'react';
import { TouchableHighlight } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { ActionsProps } from '@pirateradio/react-native-gifted-chat';

const InputActions: React.FC<ActionsProps> = (props) => {
  return (
    <TouchableHighlight
      style={{
        width: 44,
        height: 44,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 4,
        marginRight: 4,
        marginBottom: 0,
      }}
      underlayColor="crimson"
    >
      <Ionicons size={32} name="mic" {...props} />
    </TouchableHighlight>
  );
};

export default React.memo(InputActions);
