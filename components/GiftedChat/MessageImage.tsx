import * as React from 'react';
import { StyleSheet, ImageProps, ViewStyle, StyleProp, ImageStyle, View, useWindowDimensions } from 'react-native';
import { PMessage } from '../../types';
import CachedImage from '../CachedImage';
import ImageScrollView from '../ImageScrollView';

export interface MessageImageProps<TMessage extends PMessage> {
  currentMessage?: TMessage;
  containerStyle?: StyleProp<ViewStyle>;
  imageStyle?: StyleProp<ImageStyle>;
  imageProps?: Partial<ImageProps>;
}

const MessageImage: React.FC<MessageImageProps<PMessage>> = ({
  containerStyle,
  imageProps,
  imageStyle,
  currentMessage,
}) => {
  if (!currentMessage) return null;
  const images = React.useMemo(() => currentMessage.attachments.filter(({ type }) => type === 'image'), [
    currentMessage.attachments,
  ]);
  if (!images.length) return null;

  const contentWidth = useWindowDimensions().width - 120;

  return (
    <View style={[styles.container, containerStyle]}>
      <ImageScrollView images={images} contentWidth={contentWidth} sensitive={currentMessage.sensitive} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    maxHeight: 140,
  },
  image: {
    width: 150,
    height: 100,
    borderRadius: 13,
    margin: 3,
    resizeMode: 'cover',
  },
});

export default MessageImage;
