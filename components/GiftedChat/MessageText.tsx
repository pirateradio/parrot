import React from 'react';
import { StyleSheet, View, useWindowDimensions } from 'react-native';
import PleromaText from '../PleromaText';
import * as Linking from 'expo-linking';

import { PMessage } from '../../types';
import PleromaHTML from '../PleromaHTML';

const WWW_URL_PATTERN = /^www\./i;

export interface MessageTextProps<TMessage extends PMessage> {
  position: 'left' | 'right';
  optionTitles?: string[];
  currentMessage?: TMessage;
}

const MessageText: React.FC<MessageTextProps<PMessage>> = ({
  position = 'left',
  currentMessage = { text: '', emojis: [] },
}) => {
  const onUrlPress = React.useCallback((url: string) => {
    // When someone sends a message that includes a website address beginning with "www." (omitting the scheme),
    // react-native-parsed-text recognizes it as a valid url, but Linking fails to open due to the missing scheme.
    if (WWW_URL_PATTERN.test(url)) {
      onUrlPress(`http://${url}`);
    } else {
      Linking.canOpenURL(url).then((supported) => {
        if (!supported) {
          console.error('No handler for URL:', url);
        } else {
          Linking.openURL(url);
        }
      });
    }
  }, []);
  const contentWidth = useWindowDimensions().width - 120;

  return (
    <View style={styles[position].container}>
      <PleromaHTML
        content={currentMessage.text}
        emojis={currentMessage.emojis}
        contentWidth={contentWidth}
        baseStyle={styles[position].text}
      />
    </View>
  );
};

const styles = {
  left: StyleSheet.create({
    container: {
      paddingHorizontal: 5,
      paddingBottom: 5,
    },
    text: {
      fontSize: 16,
      lineHeight: 20,
      marginTop: 5,
      marginBottom: 5,
      marginLeft: 10,
      marginRight: 10,
      fontWeight: '500',
    },
  }),
  right: StyleSheet.create({
    container: {},
    text: {
      fontSize: 16,
      lineHeight: 20,
      marginTop: 5,
      marginBottom: 5,
      marginLeft: 10,
      marginRight: 10,
      fontWeight: '500',
    },
  }),
};

export default MessageText;
