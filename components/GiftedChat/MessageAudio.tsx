import * as React from 'react';
import { StyleSheet, ViewStyle, StyleProp, View } from 'react-native';
import { PMessage } from '../../types';
import AudioPlayer from '../AudioPlayer';

export interface MessageAudioProps<TMessage extends PMessage> {
  currentMessage?: TMessage;
  containerStyle?: StyleProp<ViewStyle>;
  audioStyle?: StyleProp<ViewStyle>;
  audioProps?: {
    [key: string]: any;
  };
}

const MessageAudio: React.FC<MessageAudioProps<PMessage>> = ({
  currentMessage,
  containerStyle,
  audioStyle,
  audioProps = {},
}) => {
  if (!currentMessage) return null;
  const audios = React.useMemo(() => currentMessage.attachments.filter(({ type }) => type === 'audio'), [
    currentMessage.attachments,
  ]);
  if (!audios.length) return null;

  return (
    <View style={{ height: audios.length * 35 }}>
      {audios.map((audio) => (
        <AudioPlayer key={audio.id} audio={audio} />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default MessageAudio;
