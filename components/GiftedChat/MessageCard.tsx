import * as React from 'react';
import { StyleSheet, ViewStyle, StyleProp, View, useWindowDimensions } from 'react-native';
import { PMessage } from '../../types';
import StatusCard from '../StatusCard';

export interface MessageCardProps<TMessage extends PMessage> {
  currentMessage?: TMessage;
  containerStyle?: StyleProp<ViewStyle>;
}

const MessageImage: React.FC<MessageCardProps<PMessage>> = ({ containerStyle, currentMessage }) => {
  if (!currentMessage || !currentMessage.card) return null;

  return (
    <View style={[styles.container, containerStyle]}>
      <StatusCard card={currentMessage.card} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 80,
  },
});

export default MessageImage;
