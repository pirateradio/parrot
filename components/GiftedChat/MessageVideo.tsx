import * as React from 'react';
import { StyleSheet, ViewStyle, StyleProp, View, useWindowDimensions } from 'react-native';
import { PMessage } from '../../types';
import VideoPlayer from '../VideoPlayer';

export interface MessageVideoProps<TMessage extends PMessage> {
  currentMessage?: TMessage;
  containerStyle?: StyleProp<ViewStyle>;
  videoStyle?: StyleProp<ViewStyle>;
  videoProps?: {
    [key: string]: any;
  };
}

const MessageVideo: React.FC<MessageVideoProps<PMessage>> = ({
  currentMessage,
  containerStyle,
  videoStyle,
  videoProps = {},
}) => {
  if (!currentMessage) return null;
  const videos = React.useMemo(() => currentMessage.attachments.filter(({ type }) => type === 'video'), [
    currentMessage.attachments,
  ]);
  if (!videos.length) return null;

  const contentWidth = useWindowDimensions().width - 120;

  return (
    <View style={[styles.container, containerStyle]}>
      {videos.map((video) => (
        <VideoPlayer key={video.id} video={video} contentWidth={contentWidth} />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default MessageVideo;
