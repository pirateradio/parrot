import * as React from 'react';
import { StyleSheet, TouchableWithoutFeedback, StyleProp, View as DefaultView } from 'react-native';
import { View, Text, ViewProps, TextProps } from '../Themed';

import { QuickRepliesProps, Time, User, LeftRightStyle, Reply, Omit } from '@pirateradio/react-native-gifted-chat';
import { isSameDay, isSameUser } from '@pirateradio/react-native-gifted-chat/lib/utils';
import { PMessage } from '../../types';
import MessageText, { MessageTextProps } from './MessageText';
import MessageVideo, { MessageVideoProps } from './MessageVideo';
import MessageAudio, { MessageAudioProps } from './MessageAudio';
import MessageImage, { MessageImageProps } from './MessageImage';
import MessageCard from './MessageCard';
import TimeAgo from '../TimeAgo';

export interface BubbleProps<TMessage extends PMessage> {
  user?: User;
  touchableProps?: {
    [key: string]: any;
  };
  renderUsernameOnMessage?: boolean;
  isCustomViewBottom?: boolean;
  inverted?: boolean;
  position: 'left' | 'right';
  currentMessage?: TMessage;
  nextMessage?: TMessage;
  previousMessage?: TMessage;
  optionTitles?: string[];
  containerStyle?: LeftRightStyle<ViewProps['style']>;
  wrapperStyle?: LeftRightStyle<ViewProps['style']>;
  textStyle?: LeftRightStyle<ViewProps['style']>;
  bottomContainerStyle?: LeftRightStyle<ViewProps['style']>;
  tickStyle?: StyleProp<ViewProps['style']>;
  containerToNextStyle?: LeftRightStyle<ViewProps['style']>;
  containerToPreviousStyle?: LeftRightStyle<ViewProps['style']>;
  usernameStyle?: TextProps['style'];
  quickReplyStyle?: StyleProp<ViewProps['style']>;
  onLongPress?(context?: any, message?: any): void;
  onQuickReply?(replies: Reply[]): void;
  renderMessageImage?(props: RenderMessageImageProps<TMessage>): React.ReactNode;
  renderMessageVideo?(props: RenderMessageVideoProps<TMessage>): React.ReactNode;
  renderMessageAudio?(props: RenderMessageAudioProps<TMessage>): React.ReactNode;
  renderMessageText?(props: RenderMessageTextProps<TMessage>): React.ReactNode;
  renderCustomView?(bubbleProps: BubbleProps<TMessage>): React.ReactNode;
  renderTime?(timeProps: Time['props']): React.ReactNode;
  renderTicks?(currentMessage: TMessage): React.ReactNode;
  renderUsername?(): React.ReactNode;
  renderQuickReplySend?(): React.ReactNode;
  renderQuickReplies?(quickReplies: QuickRepliesProps): React.ReactNode;
}

export type RenderMessageImageProps<TMessage extends PMessage> = Omit<
  BubbleProps<TMessage>,
  'containerStyle' | 'wrapperStyle'
> &
  MessageImageProps<TMessage>;

export type RenderMessageVideoProps<TMessage extends PMessage> = Omit<
  BubbleProps<TMessage>,
  'containerStyle' | 'wrapperStyle'
> &
  MessageVideoProps<TMessage>;

export type RenderMessageAudioProps<TMessage extends PMessage> = Omit<
  BubbleProps<TMessage>,
  'containerStyle' | 'wrapperStyle'
> &
  MessageAudioProps<TMessage>;

export type RenderMessageTextProps<TMessage extends PMessage> = Omit<
  BubbleProps<TMessage>,
  'containerStyle' | 'wrapperStyle'
> &
  MessageTextProps<TMessage>;

const Bubble: React.FC<BubbleProps<PMessage>> = (props) => {
  const {
    touchableProps = {},
    onLongPress = null,
    position = 'left',
    currentMessage = null,
    nextMessage = null,
    previousMessage = null,
  } = props;
  const _onLongPress = React.useCallback(() => onLongPress && onLongPress(currentMessage), [currentMessage]);

  const sameUserAsNextMessage = React.useMemo(
    () =>
      currentMessage &&
      nextMessage &&
      position &&
      isSameUser(currentMessage, nextMessage) &&
      isSameDay(currentMessage, nextMessage),
    []
  );

  const sameUserAsLastMessage = React.useMemo(
    () =>
      currentMessage &&
      previousMessage &&
      position &&
      isSameUser(currentMessage, previousMessage) &&
      isSameDay(currentMessage, previousMessage),
    []
  );

  const styledBubbleToNext = React.useMemo(() => {
    if (sameUserAsNextMessage) {
      return styles[position].containerToNext;
    }
    return null;
  }, []);

  const styledBubbleToPrevious = React.useMemo(() => {
    if (sameUserAsLastMessage) {
      return styles[position].containerToPrevious;
    }
    return null;
  }, []);

  const renderMessageText = React.useMemo(
    () => () => {
      if (props.currentMessage && props.currentMessage.text) {
        const { containerStyle, wrapperStyle, optionTitles, ...messageTextProps } = props;
        return <MessageText {...messageTextProps} />;
      }
      return null;
    },
    []
  );

  const renderMessageAudio = React.useMemo(
    () => () => {
      if (props.currentMessage && props.currentMessage.attachments.some(({ type }) => type === 'audio')) {
        const { containerStyle, wrapperStyle, optionTitles, ...messageAudioProps } = props;
        return <MessageAudio {...messageAudioProps} />;
      }
      return null;
    },
    []
  );

  const renderMessageVideo = React.useMemo(
    () => () => {
      if (props.currentMessage && props.currentMessage.attachments.some(({ type }) => type === 'video')) {
        const { containerStyle, wrapperStyle, optionTitles, ...messageVideoProps } = props;
        return <MessageVideo {...messageVideoProps} />;
      }
      return null;
    },
    []
  );

  const renderMessageImage = React.useMemo(
    () => () => {
      if (props.currentMessage && props.currentMessage.attachments.some(({ type }) => type === 'image')) {
        const { containerStyle, wrapperStyle, optionTitles, ...messageImageProps } = props;
        return <MessageImage {...messageImageProps} />;
      }
      return null;
    },
    []
  );

  const renderMessageCard = React.useMemo(
    () => () => {
      if (props.currentMessage && props.currentMessage.card) {
        const { containerStyle, wrapperStyle } = props;
        return <MessageCard currentMessage={props.currentMessage} />;
      }
      return null;
    },
    []
  );

  const renderUsername = React.useMemo(
    () => () => {
      const { currentMessage, user } = props;
      if (currentMessage) {
        if (user && currentMessage.user._id === user._id) {
          return null;
        }
        return (
          <DefaultView style={styles.content.usernameView}>
            {sameUserAsLastMessage ? null : (
              <Text style={styles.content.username} colorName="secondary" numberOfLines={1}>
                {currentMessage.user.name}
              </Text>
            )}
          </DefaultView>
        );
      }
      return null;
    },
    []
  );

  const renderTime = React.useMemo(
    () => () => {
      if (currentMessage && currentMessage.createdAt) {
        const { containerStyle, wrapperStyle, textStyle, ...timeProps } = props;
        return (
          <DefaultView style={styles[position].time}>
            <TimeAgo date={new Date(currentMessage.createdAt)} size={12} />
          </DefaultView>
        );
      }
      return null;
    },
    []
  );

  return (
    <DefaultView style={styles[position].container}>
      {renderUsername()}
      <View
        style={[styles[position].wrapper, styledBubbleToNext, styledBubbleToPrevious]}
        colorName={position === 'left' ? 'base3' : 'base1'}
      >
        <TouchableWithoutFeedback onLongPress={_onLongPress} accessibilityRole="text" {...touchableProps}>
          <DefaultView>
            <DefaultView>
              {renderMessageText()}
              {renderMessageImage()}
              {renderMessageVideo()}
              {renderMessageAudio()}
              {renderMessageCard()}
            </DefaultView>
            <DefaultView style={styles[position].bottom}>{renderTime()}</DefaultView>
          </DefaultView>
        </TouchableWithoutFeedback>
      </View>
    </DefaultView>
  );
};

const styles = {
  left: StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'flex-start',
    },
    wrapper: {
      padding: 5,
      borderRadius: 15,
      marginRight: 60,
      marginTop: 5,
      minHeight: 20,
      justifyContent: 'flex-end',
    },
    containerToNext: {
      borderBottomLeftRadius: 3,
    },
    containerToPrevious: {
      borderTopLeftRadius: 3,
      marginTop: 0,
    },
    bottom: {
      marginTop: 5,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    time: {
      flex: 1,
      paddingRight: 2,
      alignItems: 'flex-end',
    },
  }),
  right: StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'flex-end',
      marginTop: 10,
    },
    wrapper: {
      borderRadius: 15,
      padding: 5,
      marginLeft: 60,
      minHeight: 20,
      justifyContent: 'flex-end',
    },
    containerToNext: {
      borderBottomRightRadius: 3,
    },
    containerToPrevious: {
      borderTopRightRadius: 3,
    },
    bottom: {
      marginTop: 5,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    time: {
      paddingLeft: 7,
    },
  }),
  content: StyleSheet.create({
    tick: {
      fontSize: 10,
    },
    tickView: {
      flexDirection: 'row',
      marginRight: 10,
    },
    username: {
      left: 0,
      fontSize: 12,
      backgroundColor: 'transparent',
      fontWeight: '500',
      // color: '#aaa',
    },
    usernameView: {
      flex: 1,
      top: 2,
      flexDirection: 'row',
      marginHorizontal: 8,
      marginTop: 10,
    },
  }),
};

export default Bubble;
