const presets = ['babel-preset-expo'];
const plugins = [];

if (process.env['ENV'] === 'prod') plugins.push('transform-remove-console');

module.exports = function (api) {
  api.cache(true);
  return { presets, plugins };
};
