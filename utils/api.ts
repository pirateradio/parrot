import queryString from 'query-string';
import * as Sentry from 'sentry-expo';

export interface ApiCallOptions {
  params?: {
    [key: string]: string | number | boolean;
  };
  method?: string;
}

const callApi = async <T>(
  baseUrl: string,
  endpoint: string,
  accessToken: string,
  options: ApiCallOptions = {}
): Promise<T> => {
  const { params = {}, method = 'GET' } = options;
  try {
    const apiUrl = `${baseUrl}/api/v1`;
    const headers = { Authorization: `Bearer ${accessToken}` };
    const response = await fetch(`${apiUrl}/${endpoint}?${queryString.stringify(params)}`, { method, headers });
    if (!response.ok) throw new Error(response.statusText);
    return response.json() as Promise<T>;
  } catch (err) {
    Sentry.Native.captureException(err, {
      extra: {
        endpoint: `${endpoint}?${queryString.stringify(params)}`,
      },
    });
    throw new Error(err.message);
  }
};

export default callApi;
