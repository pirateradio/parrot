import { IMessage } from '@pirateradio/react-native-gifted-chat';

export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  Login: undefined;
};

export type BottomTabParamList = {
  Timeline: undefined;
  Chats: undefined;
  Search: undefined;
  Notifications: undefined;
};

export type TimelineTabParamList = {
  Timeline: undefined;
  TimelineOptions: undefined;
  Conversation: {
    convoId: ConversationID;
    focusConvoId?: ConversationID;
  };
};

export type ChatsTabParamList = {
  ChatsList: undefined;
  Chat: {
    chatId: ChatID;
  };
};

export type SearchTabParamList = {
  Search: undefined;
};

export type NotificationsTabParamList = {
  NotificationsList: undefined;
};

export type Emoji = {
  shortcode: string;
  static_url: string;
  url: string;
  visible_in_picker: boolean;
};

export type UserAccount = {
  id: string;
  acct: string;
  username: string;
  avatar: string;
  avatar_static: string;
  bot: boolean;
  created_at: string;
  display_name: string;
  emojis: Emoji[];
  fields: {
    name: string;
    value: string;
  }[];
  follow_requests_count: number;
  followers_count: number;
  following_count: number;
  header: string;
  header_static: string;
  locked: boolean;
  note: string;
  pleroma?: {
    accepts_chat_messages: boolean;
    /** whether the user allows automatically follow moved following accounts */
    allow_following_move: boolean;
    ap_id: string;
    /** Favicon image of the user's instance */
    favicon: string | null;
    background_image: string | null;
    chat_token: string;
    /** whether the user account is waiting on email confirmation to be activated */
    is_confirmed: boolean;
    hide_favorites: boolean;
    /** whether the user has follower hiding enabled */
    hide_followers: boolean;
    /** whether the user has follower stat hiding enabled */
    hide_followers_count: boolean;
    /** whether the user has follow hiding enabled */
    hide_follows: boolean;
    /** whether the user has follow stat hiding enabled */
    hide_follows_count: boolean;
    /** whether the user is an admin of the local instance */
    is_admin: boolean;
    /** whether the user is a moderator of the local instance */
    is_moderator: boolean;
    notification_settings: {
      block_from_strangers: boolean;
      hide_notification_contents: boolean;
    };
    relationship: {
      blocked_by: boolean;
      blocking: boolean;
      domain_blocking: boolean;
      endorsed: boolean;
      followed_by: boolean;
      following: boolean;
      id: string;
      muting: boolean;
      muting_notifications: boolean;
      requested: boolean;
      showing_reblog: boolean;
      subscribing: boolean;
    } | null;
    skip_thread_containment: boolean;
    /** The count of unread conversations. Only returned to the account owner. */
    unread_conversation_count: number;
    unread_notifications_count: number;
  };
  source: {
    /** Plaintext version of the bio without formatting applied by the backend, used for editing the bio. */
    note: string;
    privacy: 'direct' | 'public' | 'private' | 'unlisted'; // | 'local';
    sensitive: boolean;
    pleroma: {
      actor_type: 'Application' | 'Group' | 'Organization' | 'Person' | 'Service';
      /** whether the user allows indexing / listing of the account by external services (search engines etc.). */
      discoverable: boolean;
      /** whether the HTML tags for rich-text formatting are stripped from all statuses requested from the API. */
      no_rich_text: boolean;
      /** whether the user wants their role (e.g admin, moderator) to be shown */
      show_role: boolean;
    };
  };
  statuses_count: number;
  url: string;
};

export type Instance = {
  approval_required: boolean;
  avatar_upload_limit: number;
  background_image: string;
  background_upload_limit: number;
  banner_upload_limit: number;
  chat_limit: number;
  description: string;
  description_limit: 5000;
  email: string;
  languages: string[];
  max_toot_chars: 5000;
  pleroma?: {
    metadata: {
      account_activation_required: boolean;
      features: string[];
      federation: {
        enabled: boolean;
      };
      fields_limits: {
        max_fields: number;
        max_remote_fields: 20;
        name_length: 512;
        value_length: 2048;
      };
      port_formats: string[];
    };
    vapid_public_key: string;
  };
  poll_limits: {
    max_expiration: number;
    max_option_chars: 200;
    max_options: 20;
    min_expiration: 0;
  };
  registrations: boolean;
  stats: {
    domain_count: number;
    status_count: number;
    user_count: number;
  };
  thumbnail: string;
  title: string;
  upload_limit: number;
  uri: string;
  urls: {
    any: string;
  };
  version: string;
};

export type InstancePeers = string[];

/** Represents a file or media attachment that can be added to a status. */
export type MediaAttachment = {
  /** The ID of the attachment in the database. */
  id: string;
  /** Alternate text that describes what is in the media attachment, to be used for the visually impaired or when media attachments do not load */
  description: string | null;
  pleroma: {
    /** mime type of the attachment */
    mime_type: string;
  };
  /** The location of a scaled-down preview of the attachment */
  preview_url: string;
  /** The location of the full-size original attachment on the remote website. String (URL), or null if the attachment is local */
  remote_url: string | null;
  /** A shorter URL for the attachment */
  text_url: string;
  /** The type of the attachmen */
  type: 'video' | 'audio' | 'image' | 'unknown';
  /** The location of the original full-size attachment */
  url: string;
};

export type Mention = {
  acct: UserAccount['acct'];
  id: UserAccount['id'];
  url: UserAccount['url'];
  username: UserAccount['username'];
};

export type MediaCard = {
  description: string;
  image: string | null;
  provider_name: string | null;
  provider_url: string;
  title: string;
  type: 'link' | 'photo' | 'video' | 'rich';
  url: string;
  pleroma?: {
    opengraph: {
      [key: string]: string;
    };
  };
};

export type Status = {
  id: string;
  /** ID of the account being replied to */
  in_reply_to_account_id: UserAccount['id'] | null;
  /** ID of the status being replied to */
  in_reply_to_id: Status['id'] | null;
  /** The account that authored this status */
  account: UserAccount;
  /** The application used to post this status */
  application: {
    name: string;
    website: string | null;
  };
  /** Have you bookmarked this status? */
  bookmarked: boolean;
  /** Preview card for links included within status content */
  card: MediaCard | null;
  /** HTML-encoded status content */
  content: string;
  /** The date when this status was created */
  created_at: string;
  /** Custom emoji to be used when rendering status content */
  emojis: Emoji[];
  /** Have you favourited this status? */
  favourited: boolean;
  /** How many favourites this status has received */
  favourites_count: number;
  /** Primary language of this status */
  language: string | null;
  /** Media that is attached to this status */
  media_attachments: MediaAttachment[];
  /** Mentions of users within the status content */
  mentions: Mention[];
  /** Have you muted notifications for this status's conversation? */
  muted: boolean;
  /** Have you pinned this status? Only appears if the status is pinnable. */
  pinned: boolean | undefined;
  pleroma: {
    /** A map consisting of alternate representations of the `content` property with the key being it's mimetype. Currently the only alternate representation supported is `text/plain` */
    content: {
      'text/plain'?: string;
    };
    /** The ID of the AP context the status is associated with (if any) */
    conversation_id: number;
    /** The ID of the Mastodon direct message conversation the status is associated with (if any) */
    direct_conversation_id: number | undefined;
    /** A list with emoji / reaction maps. Contains no information about the reacting users, for that use the /statuses/:id/reactions endpoint. */
    emoji_reactions: {
      name: string;
      count: number;
      me: boolean;
    }[];
    /** A datetime (ISO 8601) that states when the post will expire (be deleted automatically), or empty if the post won't expire */
    expires_at: string | null;
    /** The `acct` property of User entity for replied user (if any) */
    in_reply_to_account_acct: UserAccount['acct'] | null;
    /** `true` if the post was made on the local instance */
    local: boolean;
    /** `true` if the parent post is visible to the user */
    parent_visible: boolean;
    /** A map consisting of alternate representations of the `spoiler_text` property with the key being it's mimetype. Currently the only alternate representation supported is `text/plain`. */
    spoiler_text: {
      'text/plain'?: string;
    };
    /** `true` if the thread the post belongs to is muted */
    thread_muted: boolean;
  };
  /** The poll attached to the status */
  poll: {
    /** Custom emoji to be used for rendering poll options. */
    emojis: Emoji[];
    /** Is the poll currently expired? */
    expired: boolean;
    /** When the poll ends */
    expires_at: string | null;
    id: string;
    /** Does the poll allow multiple-choice answers? */
    multiple: boolean;
    /** Possible answers for the poll. */
    options: {
      title: string;
      votes_count: number;
    }[];
    /** When called with a user token, has the authorized user voted? Boolean, or null if no current user. */
    voted: boolean | null;
    /** How many unique accounts have voted. Number. */
    voters_count: number;
    /** How many votes have been received. Number. */
    votes_count: number;
  } | null;
  /** The status being reblogged */
  reblog: Status | null;
  /** Have you boosted this status? */
  reblogged: boolean;
  /** How many boosts this status has received */
  reblogs_count: number;
  /** How many replies this status has received */
  replies_count: number;
  /** "Is this status marked as sensitive content?" */
  sensitive: boolean;
  /** "Subject or summary line, below which status content is collapsed until expanded" */
  spoiler_text: string;
  tags: {
    name: string;
    url: string;
  }[];
  /** URI of the status used for federation */
  uri: string;
  /** A link to the status's HTML representation */
  url: string;
  /** Visibility of this status */
  visibility: 'direct' | 'public' | 'private' | 'unlisted'; // | 'local'
};

export type StatusContext = {
  ancestors: Status[];
  descendants: Status[];
};

export type ConversationID = Status['id'];

export type ChatMessage = {
  /** The Mastodon API id of the actor */
  account_id: UserAccount['id'];
  attachment: MediaAttachment | null;
  card: MediaCard | null;
  chat_id: Chat['id'];
  content: string | null;
  created_at: string;
  emojis: Emoji[];
  id: string;
  unread: boolean;
  idempotency_key?: string; // https://docs-develop.pleroma.social/backend/development/API/chats/#getting-the-messages-for-a-chat
};

export type Chat = {
  id: string;
  account: UserAccount;
  last_message: ChatMessage | null;
  unread: number;
  updated_at: string;
};

export type ChatID = Chat['id'];

export type Notification = {
  account: UserAccount;
  created_at: string;
  id: string;
  pleroma: {
    is_muted: boolean;
    if_seen: boolean;
  };
  status: Status;
  type:
    | 'move'
    | 'mention'
    | 'pleroma:emoji_reaction'
    | 'pleroma:chat_mention'
    | 'pleroma:report'
    | 'follow'
    | 'favourite'
    | 'reblog'
    | 'poll'
    | 'follow_request';
};

export type NotificationID = Notification['id'];

export interface PMessage extends IMessage {
  user: IMessage['user'] & {
    emojis: Emoji[];
  };
  attachments: MediaAttachment[];
  mentions: Mention[];
  emojis: Emoji[];
  card: MediaCard | null;
  sensitive: Status['sensitive'];
}
