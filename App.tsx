import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import * as SplashScreen from 'expo-splash-screen';
SplashScreen.preventAutoHideAsync(); // this needs to be called early
import * as Sentry from 'sentry-expo';
import { ModalProvider } from 'react-native-use-modal-hooks';
import { ActionSheetProvider } from '@expo/react-native-action-sheet';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

import { auth, AuthContext } from './stores/auth';
import { user, UserContext } from './stores/user';
import { search, SearchContext } from './stores/search';

Sentry.init({
  dsn: 'https://962e553238fc4d4db8632baea65f80e0@o518719.ingest.sentry.io/5628070',
});

export default function App(): JSX.Element | null {
  const [ready, setReady] = React.useState(false);
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  React.useEffect(() => {
    const init = async () => {
      await auth.init();
      if (auth.loggedIn) {
        await user.init();
        setTimeout(SplashScreen.hideAsync, 300);
      }
      setReady(true);
    };
    init();
  }, []);

  if (!isLoadingComplete || !ready) {
    return null;
  } else {
    return (
      <Sentry.Native.ErrorBoundary fallback="An error has occured">
        <SafeAreaProvider>
          <ActionSheetProvider>
            <AuthContext.Provider value={auth}>
              <UserContext.Provider value={user}>
                <SearchContext.Provider value={search}>
                  <ModalProvider>
                    <Navigation colorScheme={colorScheme} />
                  </ModalProvider>
                </SearchContext.Provider>
              </UserContext.Provider>
            </AuthContext.Provider>
          </ActionSheetProvider>
          <StatusBar />
        </SafeAreaProvider>
      </Sentry.Native.ErrorBoundary>
    );
  }
}
