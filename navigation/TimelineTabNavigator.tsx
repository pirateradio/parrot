import * as React from 'react';
import {
  createStackNavigator,
  HeaderBackButton,
  CardStyleInterpolators,
  HeaderStyleInterpolators,
} from '@react-navigation/stack';
import { timeline } from '../stores/timeline';
import { useThemeColor } from '../components/Themed';
import { TimelineTabParamList } from '../types';
import TimelineScreen from '../screens/TimelineScreen';
import ConversationScreen from '../screens/ConversationScreen';
import TimelineOptionsScreen from '../screens/TimelineOptionsScreen';
import PressableIcon from '../components/PressableIcon';

const TimelineTabStack = createStackNavigator<TimelineTabParamList>();

const TimelineTabNavigator: React.FC = () => {
  const backgroundColor = useThemeColor({}, 'base1');
  const textColor = useThemeColor({}, 'primary');

  return (
    <TimelineTabStack.Navigator
      mode="card"
      headerMode="float"
      screenOptions={{
        headerStyle: { backgroundColor },
        headerTintColor: textColor,
        headerTitleAlign: 'center',
        cardOverlayEnabled: true,
        gestureEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,
      }}
    >
      <TimelineTabStack.Screen
        name="Timeline"
        component={TimelineScreen}
        options={({ navigation }) => ({
          headerTitle: `${timeline.timelineOption.charAt(0).toUpperCase()}${timeline.timelineOption.slice(1)}`,
          headerRight: () => (
            <PressableIcon
              bgColorName="base1"
              name="ios-add"
              size={30}
              onPress={() => console.log('pressed new post button')}
              style={{ marginRight: 5 }}
              hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}
            />
          ),
          headerLeft: () => (
            <PressableIcon
              bgColorName="base1"
              name="ios-menu-outline"
              onPress={() => navigation.navigate('TimelineOptions')}
              style={{ marginLeft: 10 }}
              hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}
            />
          ),
        })}
      />

      <TimelineTabStack.Screen
        name="Conversation"
        component={ConversationScreen}
        options={{ headerTitle: 'Conversation' }}
      />
      <TimelineTabStack.Screen
        name="TimelineOptions"
        component={TimelineOptionsScreen}
        options={({ navigation }) => ({
          headerTitle: 'Options',
          gestureDirection: 'horizontal-inverted',
          headerLeft: () => null,
          headerBackTitle: '',
          headerStyleInterpolator: HeaderStyleInterpolators.forSlideRight,
          headerRight: (props) => (
            <HeaderBackButton
              label="Timeline"
              style={{ flexDirection: 'row-reverse' }}
              onPress={() => navigation.navigate('Timeline')}
              tintColor={textColor}
              {...props}
            />
          ),
        })}
      />
    </TimelineTabStack.Navigator>
  );
};

export default TimelineTabNavigator;
