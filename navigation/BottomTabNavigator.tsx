import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import { BottomTabParamList } from '../types';

import TimelineTabNavigator from './TimelineTabNavigator';
import ChatsTabNavigator from './ChatsTabNavigator';
import SearchTabNavigator from './SearchTabNavigator';
import NotificationsTabNavigator from './NotificationsTabNavigator';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

const BottomTabNavigator: React.FC = () => {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      lazy={false}
      initialRouteName="Timeline"
      tabBarOptions={{
        activeTintColor: Colors[colorScheme].primary,
        inactiveTintColor: Colors[colorScheme].inactive,
        style: { backgroundColor: Colors[colorScheme].base1 },
      }}
    >
      <BottomTab.Screen
        name="Timeline"
        component={TimelineTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-home-outline" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Chats"
        component={ChatsTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-chatbubbles-outline" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Search"
        component={SearchTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-search-outline" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Notifications"
        component={NotificationsTabNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-notifications-outline" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
};

export interface TabBarIconProps {
  name: React.ComponentProps<typeof Ionicons>['name'];
  color: string;
}

const TabBarIcon: React.FC<TabBarIconProps> = (props) => {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
};

export default BottomTabNavigator;
