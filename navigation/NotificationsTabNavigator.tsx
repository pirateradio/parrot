import * as React from 'react';
import { createStackNavigator, CardStyleInterpolators, HeaderStyleInterpolators } from '@react-navigation/stack';
import { useThemeColor } from '../components/Themed';
import { NotificationsTabParamList } from '../types';
import NotificationsListScreen from '../screens/NotificationsListScreen';

const NotificationsTabStack = createStackNavigator<NotificationsTabParamList>();

const NotificationsTabNavigator: React.FC = () => {
  const backgroundColor = useThemeColor({}, 'base1');
  const textColor = useThemeColor({}, 'primary');

  return (
    <NotificationsTabStack.Navigator
      mode="card"
      headerMode="float"
      screenOptions={{
        headerStyle: { backgroundColor },
        headerTintColor: textColor,
        headerTitleAlign: 'center',
        cardOverlayEnabled: true,
        gestureEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,
      }}
    >
      <NotificationsTabStack.Screen
        name="NotificationsList"
        component={NotificationsListScreen}
        options={{ headerTitle: 'Notifications' }}
      />
    </NotificationsTabStack.Navigator>
  );
};

export default NotificationsTabNavigator;
