import * as React from 'react';
import { createStackNavigator, CardStyleInterpolators, HeaderStyleInterpolators } from '@react-navigation/stack';
import { useThemeColor } from '../components/Themed';
import { SearchTabParamList } from '../types';
import SearchScreen from '../screens/SearchScreen';
import PressableIcon from '../components/PressableIcon';

const SearchTabStack = createStackNavigator<SearchTabParamList>();

const SearchTabNavigator: React.FC = () => {
  const backgroundColor = useThemeColor({}, 'base1');
  const textColor = useThemeColor({}, 'primary');

  return (
    <SearchTabStack.Navigator
      mode="card"
      headerMode="float"
      screenOptions={{
        headerStyle: { backgroundColor },
        headerTintColor: textColor,
        headerTitleAlign: 'center',
        cardOverlayEnabled: true,
        gestureEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,
      }}
    >
      <SearchTabStack.Screen
        name="Search"
        component={SearchScreen}
        options={({ navigation }) => ({
          headerTitle: 'Search',
          headerRight: () => (
            <PressableIcon
              bgColorName="base1"
              name="ios-filter-outline"
              size={30}
              onPress={() => console.log('pressed search settings button')}
              style={{ marginRight: 5 }}
              hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}
            />
          ),
        })}
      />
    </SearchTabStack.Navigator>
  );
};

export default SearchTabNavigator;
