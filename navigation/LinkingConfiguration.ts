import { LinkingOptions } from '@react-navigation/native';
import * as Linking from 'expo-linking';

const LinkingConfiguration: LinkingOptions = {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          TimelineTab: {
            screens: {
              TimelineScreen: 'timeline',
            },
          },
          ChatsTab: {
            screens: {
              ChatsScreen: 'chats',
            },
          },
          NotificationsTab: {
            screens: {
              NotificationsScreen: 'notifications',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};

export default LinkingConfiguration;
