import * as React from 'react';
import { createStackNavigator, CardStyleInterpolators, HeaderStyleInterpolators } from '@react-navigation/stack';
import { useThemeColor } from '../components/Themed';
import PressableIcon from '../components/PressableIcon';
import { ChatsTabParamList } from '../types';
import ChatsListScreen from '../screens/ChatsListScreen';
import ChatScreen from '../screens/ChatScreen';

const ChatsTabStack = createStackNavigator<ChatsTabParamList>();

const ChatsTabNavigator: React.FC = () => {
  const backgroundColor = useThemeColor({}, 'base1');
  const textColor = useThemeColor({}, 'primary');

  return (
    <ChatsTabStack.Navigator
      mode="card"
      headerMode="float"
      screenOptions={{
        headerStyle: { backgroundColor },
        headerTintColor: textColor,
        headerTitleAlign: 'center',
        cardOverlayEnabled: true,
        gestureEnabled: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,
      }}
    >
      <ChatsTabStack.Screen
        name="ChatsList"
        component={ChatsListScreen}
        options={{
          headerTitle: 'Chats',
          headerRight: () => (
            <PressableIcon
              bgColorName="base1"
              name="ios-add"
              size={30}
              onPress={() => console.log('pressed new chat button')}
              style={{ marginRight: 5 }}
              hitSlop={{ top: 5, bottom: 5, left: 5, right: 5 }}
            />
          ),
        }}
      />
      <ChatsTabStack.Screen name="Chat" component={ChatScreen} options={{ headerTitle: 'Chat' }} />
    </ChatsTabStack.Navigator>
  );
};

export default ChatsTabNavigator;
