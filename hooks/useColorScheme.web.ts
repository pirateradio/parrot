export type UseColorSchemeWebHook = () => string;
// useColorScheme from react-native does not support web currently. You can replace
// this with react-native-appearance if you would like theme support on web.
const useColorSchemeWeb: UseColorSchemeWebHook = () => {
  return 'light';
};

export default useColorSchemeWeb;
