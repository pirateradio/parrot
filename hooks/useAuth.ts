import * as React from 'react';
import { AuthContext } from '../stores/auth';

const useUser = () => React.useContext(AuthContext);

export default useUser;
