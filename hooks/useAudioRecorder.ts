import * as React from 'react';
import * as Permissions from 'expo-permissions';
import { Audio } from 'expo-av';
import * as Sentry from 'sentry-expo';

export type UseAudioRecorderHookReturn = {
  startRecording: () => void;
  /** Returns file URI for new audio recording */
  stopRecording: () => Promise<string | null>;
};

export type UseAudioRecorderHook = () => UseAudioRecorderHookReturn;

const useAudioRecorder: UseAudioRecorderHook = () => {
  const [recording, setRecording] = React.useState<Audio.Recording>();
  const [recordPermission, askForRecordPermission] = Permissions.usePermissions(Permissions.AUDIO_RECORDING);

  const startRecording = React.useCallback(async () => {
    console.log('start recording');
    try {
      if (!recordPermission || recordPermission.status !== 'granted') {
        await askForRecordPermission();
        const { status } = await Permissions.getAsync(Permissions.AUDIO_RECORDING);
        if (status !== 'granted') return;
      }
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: true,
        playsInSilentModeIOS: true,
      });
      const recording = new Audio.Recording();
      await recording.prepareToRecordAsync({
        ios: {
          extension: '.m4a',
          outputFormat: Audio.RECORDING_OPTION_IOS_OUTPUT_FORMAT_MPEG4AAC,
          audioQuality: Audio.RECORDING_OPTION_IOS_AUDIO_QUALITY_MAX,
          sampleRate: 44100,
          numberOfChannels: 2,
          bitRate: 128000,
          linearPCMBitDepth: 16,
          linearPCMIsBigEndian: false,
          linearPCMIsFloat: false,
        },
        android: {
          extension: '.m4a',
          outputFormat: Audio.RECORDING_OPTION_ANDROID_OUTPUT_FORMAT_MPEG_4,
          audioEncoder: Audio.RECORDING_OPTION_ANDROID_AUDIO_ENCODER_AAC,
          sampleRate: 44100,
          numberOfChannels: 2,
          bitRate: 128000,
        },
      });
      await recording.startAsync();
      setRecording(recording);
    } catch (err) {
      Sentry.Native.captureException(err);
    }
  }, [recording]);

  const stopRecording = React.useMemo(
    () => async () => {
      console.log('stop recording');
      if (!recording) throw new Error('Recording never started');

      setRecording(undefined);
      await recording.stopAndUnloadAsync();
      return recording.getURI();
    },
    [recording]
  );

  return { startRecording, stopRecording };
};

export default useAudioRecorder;
