import * as React from 'react';
import useWebSocket from 'react-native-use-websocket';
import Timeline, { TimelineContext } from '../stores/timeline';
import { Status } from '../types';
import useAuth from './useAuth';

export interface UseTimelineHookOptions {
  onUpdate?: (status: Status) => void;
  onDelete?: (statusId: Status['id']) => void;
}

export interface UseTimelineHookReturn {
  timeline: Timeline;
  loading: boolean;
}

export type UseTimelineHook = (options: UseTimelineHookOptions) => UseTimelineHookReturn;

const useTimeline: UseTimelineHook = (options = {}) => {
  const auth = useAuth();
  const { onUpdate, onDelete } = options;
  const timeline = React.useContext(TimelineContext);

  React.useEffect(() => {
    if (!timeline.isInit) timeline.init();
  }, []);

  const socketUrl = React.useMemo(() => {
    const wssEndpoint = `${auth.baseUrl.replace('https://', 'wss://')}/api/v1/streaming`;
    let path = '';
    switch (timeline.timelineOption) {
      case 'home':
        path = 'user';
        break;
      case 'local':
        path = 'public:local';
        break;
      case 'public':
        path = 'public';
        break;
    }
    return `${wssEndpoint}?access_token=${auth.accessToken}&stream=${path}`;
  }, [timeline.timelineOption, auth.baseUrl, auth.accessToken]);

  // probably need to consider a websocket proxy for production release
  // also rest fallback if websockets are too unreliable
  const { lastJsonMessage } = useWebSocket(socketUrl, {
    share: true,
    onOpen: () => console.log('opened'),
    shouldReconnect: () => true,
    onError: (error) => console.log(error),
  });

  React.useEffect(() => {
    const { event, payload } = lastJsonMessage;
    if (event === 'delete' && onDelete) onDelete(payload);
    else if (event === 'update' && onUpdate) {
      const status: Status = JSON.parse(payload);
      onUpdate(status);
    }
  }, [lastJsonMessage]);

  const loading = React.useMemo(() => !timeline.isInit, [timeline.isInit]);

  return {
    timeline,
    loading,
  };
};

export default useTimeline;
