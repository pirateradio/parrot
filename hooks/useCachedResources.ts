import { Ionicons } from '@expo/vector-icons';
import { Audio } from 'expo-av';
import { INTERRUPTION_MODE_ANDROID_DO_NOT_MIX, INTERRUPTION_MODE_IOS_DO_NOT_MIX } from 'expo-av/build/Audio';
import * as Font from 'expo-font';
import * as React from 'react';

export type UseCachedResourcesHook = () => boolean;

const useCachedResources: UseCachedResourcesHook = () => {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'space-mono': require('../assets/fonts/SpaceMono-Regular.ttf'),
        });

        await Audio.setAudioModeAsync({
          interruptionModeIOS: INTERRUPTION_MODE_IOS_DO_NOT_MIX,
          interruptionModeAndroid: INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
};

export default useCachedResources;
