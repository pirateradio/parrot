import * as React from 'react';
import Search, { SearchContext } from '../stores/search';

export interface UseSearchHookReturn {
  search: Search;
  loading: boolean;
}

export type UseSearchHook = () => UseSearchHookReturn;

const useSearch: UseSearchHook = () => {
  const search = React.useContext(SearchContext);

  return { search, loading: !search.isInit || search.loading };
};

export default useSearch;
