import * as React from 'react';
import { Audio, AVPlaybackStatus } from 'expo-av';
import * as Sentry from 'sentry-expo';

export type UseAudioPlaybackHookReturn = {
  play: () => void;
  stop: () => void;
  pause: () => void;
  isPlaying: boolean;
  isBuffering: boolean;
  isLoading: boolean;
  durationMs: number | undefined;
  playableDurationMs: number | undefined;
  positionMs: number;
  setPositionMs: (newPositionMs: number) => void;
};

export type UseAudioPlaybackHookOptions = {
  url?: string;
};

export type UseAudioPlaybackHook = (options: UseAudioPlaybackHookOptions) => UseAudioPlaybackHookReturn;

const useAudioPlayback: UseAudioPlaybackHook = ({ url = '' }) => {
  const [isLoading, setLoading] = React.useState(true);
  const [clip, setClip] = React.useState<Audio.Sound>();
  const [status, _setStatus] = React.useState<AVPlaybackStatus>();
  const componentIsMounted = React.useRef(true);

  React.useEffect(() => {
    componentIsMounted.current = true;
    return () => {
      componentIsMounted.current = false;
      if (clip) clip.unloadAsync();
    };
  }, [clip]);

  const setStatus = React.useCallback((status) => {
    if (componentIsMounted.current) _setStatus(status);
  }, []);

  React.useEffect(() => {
    const loadAudio = async () => {
      try {
        if (!url) {
          return;
        }
        const { sound } = await Audio.Sound.createAsync({ uri: url });
        sound.setOnPlaybackStatusUpdate((newStatus) => setStatus(newStatus));
        setClip(sound);
        setLoading(false);
      } catch (err) {
        Sentry.Native.captureException(err);
      }
    };

    loadAudio();
  }, []);

  const play = React.useCallback(async () => {
    if (!clip) return;
    await Audio.setAudioModeAsync({
      playsInSilentModeIOS: true,
      allowsRecordingIOS: false,
    });
    clip.playAsync();
  }, [clip]);

  const pause = React.useCallback(() => {
    if (!clip) return;
    return clip.pauseAsync();
  }, [clip]);

  const stop = React.useCallback(() => {
    if (!clip) return;
    return clip.pauseAsync();
  }, [clip]);

  const isPlaying = React.useMemo(() => (status && 'isPlaying' in status ? status.isPlaying : false), [status]);

  const isBuffering = React.useMemo(() => (status && 'isBuffering' in status ? status.isBuffering : false), [status]);

  const durationMs = React.useMemo(() => (status && 'durationMillis' in status ? status.durationMillis : 0), [status]);

  const playableDurationMs = React.useMemo(
    () => (status && 'playableDurationMillis' in status ? status.playableDurationMillis : 0),
    [status]
  );

  const positionMs = React.useMemo(() => (status && 'positionMillis' in status ? status.positionMillis : 0), [status]);

  const setPositionMs = React.useCallback(
    (newPositionMs) => {
      if (!clip) return;
      clip.setStatusAsync({ shouldPlay: isPlaying, positionMillis: newPositionMs });
    },
    [isPlaying]
  );

  return {
    play,
    pause,
    stop,
    isLoading,
    isPlaying,
    isBuffering,
    durationMs,
    playableDurationMs,
    positionMs,
    setPositionMs,
  };
};

export default useAudioPlayback;
