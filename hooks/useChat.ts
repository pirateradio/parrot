import * as React from 'react';
import useWebSocket from 'react-native-use-websocket';
import Chat from '../stores/chat';
import { ChatMessage, ChatID, Chat as ChatType } from '../types';
import useAuth from './useAuth';

export interface UseChatHookOptions {
  onUpdate?: (message: ChatMessage) => void;
}

export interface UseChatHookReturn {
  chat: Chat;
  loading: boolean;
}

export type UseChatHook = (chatId: ChatID, options: UseChatHookOptions) => UseChatHookReturn;

const useChat: UseChatHook = (chatId, options = {}) => {
  const { onUpdate } = options;
  const chat = React.useMemo(() => new Chat(chatId), []);
  const auth = useAuth();

  const socketUrl = React.useMemo(() => {
    const wssEndpoint = `${auth.baseUrl.replace('https://', 'wss://')}/api/v1/streaming`;
    return `${wssEndpoint}?access_token=${auth.accessToken}&stream=user:pleroma_chat`;
  }, [auth.baseUrl, auth.accessToken]);

  const { lastJsonMessage } = useWebSocket(socketUrl, {
    share: true,
    onOpen: () => console.log('opened'),
    shouldReconnect: () => true,
    onError: (error) => console.log(error),
  });

  React.useEffect(() => {
    const { event, payload } = lastJsonMessage;
    if (event === 'pleroma:chat_update') {
      const { id, last_message } = JSON.parse(payload) as ChatType;
      if (id === chatId && last_message) {
        chat.addMessage(last_message);
        if (onUpdate) onUpdate(last_message);
      }
    }
  }, [lastJsonMessage]);

  return { chat, loading: !chat.isInit };
};

export default useChat;
