import * as React from 'react';
import Notifications, { notifications } from '../stores/notifications';

export interface UseNotificationsHookReturn {
  notifications: Notifications;
  loading: boolean;
}

export type UseNotificationsHook = () => UseNotificationsHookReturn;

// notifications apparently don't have streaming support over websockets
const useNotifications: UseNotificationsHook = () => {
  React.useEffect(() => {
    if (!notifications.isInit) notifications.init();
  }, []);

  return { notifications, loading: !notifications.isInit };
};

export default useNotifications;
