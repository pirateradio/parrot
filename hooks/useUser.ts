import * as React from 'react';
import { UserContext } from '../stores/user';

const useUser = () => React.useContext(UserContext);

export default useUser;
