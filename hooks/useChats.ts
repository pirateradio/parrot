import * as React from 'react';
import useWebSocket from 'react-native-use-websocket';
import Chats, { chats } from '../stores/chats';
import { Chat } from '../types';
import useAuth from './useAuth';

export interface UseChatsHookOptions {
  onUpdate?: (chat: Chat) => void;
}

export interface UseChatsHookReturn {
  chats: Chats;
  loading: boolean;
}

export type UseChatsHook = (options: UseChatsHookOptions) => UseChatsHookReturn;

const useChats: UseChatsHook = (options = {}) => {
  const { onUpdate } = options;
  const auth = useAuth();

  React.useEffect(() => {
    if (!chats.isInit) chats.init();
  }, []);

  const socketUrl = React.useMemo(() => {
    const wssEndpoint = `${auth.baseUrl.replace('https://', 'wss://')}/api/v1/streaming`;
    return `${wssEndpoint}?access_token=${auth.accessToken}&stream=user:pleroma_chat`;
  }, [auth.baseUrl, auth.accessToken]);

  const { lastJsonMessage } = useWebSocket(socketUrl, {
    share: true,
    onOpen: () => console.log('opened'),
    shouldReconnect: () => true,
    onError: (error) => console.log(error),
  });

  React.useEffect(() => {
    const { event, payload } = lastJsonMessage;
    if (event === 'pleroma:chat_update') {
      const chat = JSON.parse(payload);
      chats.updateChat(chat);
      if (onUpdate) onUpdate(chat);
    }
  }, [lastJsonMessage]);

  return { chats, loading: !chats.isInit };
};

export default useChats;
