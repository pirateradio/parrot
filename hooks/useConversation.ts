import * as React from 'react';
import useWebSocket from 'react-native-use-websocket';
import Conversation from '../stores/conversation';
import { Status, ConversationID } from '../types';
import useAuth from './useAuth';

export interface UseConversationHookOptions {
  onUpdate?: (status: Status) => void;
  onDelete?: (statusId: Status['id']) => void;
}

export interface UseConversationHookReturn {
  conversation: Conversation;
  loading: boolean;
}

export type UseConversationHook = (
  convoId: ConversationID,
  options: UseConversationHookOptions
) => UseConversationHookReturn;

const useConversation: UseConversationHook = (convoId, options = {}) => {
  const { onUpdate, onDelete } = options;
  const conversation = React.useMemo(() => new Conversation(convoId), []);
  const auth = useAuth();

  const socketUrl = React.useMemo(() => {
    const wssEndpoint = `${auth.baseUrl.replace('https://', 'wss://')}/api/v1/streaming`;
    return `${wssEndpoint}?access_token=${auth.accessToken}&stream=public`;
  }, [auth.baseUrl, auth.accessToken]);

  const { lastJsonMessage } = useWebSocket(socketUrl, {
    share: true,
    onOpen: () => console.log('opened'),
    shouldReconnect: () => true,
    onError: (error) => console.log(error),
  });

  React.useEffect(() => {
    const { event, payload } = lastJsonMessage;
    if (event === 'delete') {
      if (conversation.messages.some(({ id }) => id === payload)) {
        conversation.removeMessage(payload);
        if (onDelete) onDelete(payload);
      }
    } else if (event === 'update') {
      const status: Status = JSON.parse(payload);
      const { in_reply_to_id } = status;
      if (conversation.messages.some(({ id }) => id === in_reply_to_id)) {
        if (onUpdate) onUpdate(status);
        conversation.addMessage(status);
      }
    }
  }, [lastJsonMessage]);

  return { conversation, loading: !conversation.isInit };
};

export default useConversation;
